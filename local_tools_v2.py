import numpy as np
import pandas as pd
import keras
import keras.backend as K
from keras.utils import Sequence
import tensorflow as tf
import MLme as tools
from sklearn.preprocessing import Imputer, LabelEncoder
from tqdm import tqdm


def buildModel_v1a(datastructure, input_order):
    Input = keras.layers.Input
    LSTM = keras.layers.LSTM
    TimeDistributed = keras.layers.TimeDistributed
    Dense = keras.layers.Dense
    Model = keras.models.Model
    Concatenate = keras.layers.Concatenate
    Dropout = keras.layers.Dropout
    Masking = keras.layers.Masking
    Bidirectional = keras.layers.Bidirectional

    input_dict = dict()
    info_dict = dict()

    napp = datastructure['app']['n']
    Tpr = datastructure['prev']['T']
    npr = datastructure['prev']['n']
    Tcred = datastructure['cred']['T']
    ncred = datastructure['cred']['n']
    Tinst = datastructure['inst']['T']
    ninst = datastructure['inst']['n']
    Ninst = datastructure['inst']['N']
    Tpos = datastructure['pos']['T']
    npos = datastructure['pos']['n']
    Tbr = datastructure['br']['T']
    nbr = datastructure['br']['n']

    ker_init1 = keras.initializers.RandomNormal(mean=0.0, stddev=0.3, seed=0)

    inst_mask = np.zeros(shape=(ninst,))

    if 'app' in input_order:
        app_input = Input(shape=(napp,), name='app_input')
        app_info = app_input
        # app_info = Dense(256, kernel_initializer=ker_init1, activation='relu', name='dense_app_1')(app_input)
        # app_info = Dense(128, kernel_initializer=ker_init1, activation='relu', name='dense_app_2')(app_input)
        info_dict['app'] = app_info
        input_dict['app'] = app_input

    if 'prev' in input_order:
        prev_input = Input(shape=(Tpr, npr), name='prev_input')
        prev_info = prev_input
        # prev_info = TimeDistributed( Dense(256, activation='relu', kernel_initializer=ker_init1))(prev_info)
        # prev_info = TimeDistributed( Dense(128, activation='relu', kernel_initializer=ker_init1))(prev_info)
        prev_info = Bidirectional(LSTM(64, return_sequences=False, kernel_initializer=ker_init1))(prev_info)
        info_dict['prev'] = prev_info
        input_dict['prev'] = prev_input


    if 'cred' in input_order:
        cred_input = Input(shape=(Tcred, ncred), name='cred_input')
        cred_info = cred_input
        # cred_info = TimeDistributed( Dense(256, activation='relu', kernel_initializer=ker_init1))(cred_info)
        # cred_info = TimeDistributed( Dense(128, activation='relu', kernel_initializer=ker_init1))(cred_info)
        cred_info = Bidirectional(LSTM(32, return_sequences=False, kernel_initializer=ker_init1))(cred_info)
        info_dict['cred'] = cred_info
        input_dict['cred'] = cred_input


    if 'inst' in input_order:
        inst_input = Input(shape=(Tinst, Ninst, ninst), name='inst_input')
        inst_info = inst_input
        inst_info = TimeDistributed( TimeDistributed( Dense(32, activation='relu', kernel_initializer=ker_init1)))(inst_info)
        # inst_info = TimeDistributed( TimeDistributed( Dense(256, activation='relu', kernel_initializer=ker_init1)))(inst_info)
        # inst_info = TimeDistributed( Bidirectional( LSTM(32, return_sequences=True, kernel_initializer=ker_init1), merge_mode='concat'))(inst_info)
        # inst_info = TimeDistributed( Bidirectional( LSTM(128, return_sequences=True, kernel_initializer=ker_init1), merge_mode='concat'))(inst_info)
        inst_info = TimeDistributed( Bidirectional( LSTM(32, return_sequences=False, kernel_initializer=ker_init1), merge_mode='concat'))(inst_info)
        inst_info = Bidirectional( LSTM(32, return_sequences=False, kernel_initializer=ker_init1), merge_mode='concat')(inst_info)
        # inst_info = Dense(64, activation='relu', kernel_initializer=ker_init1)(inst_info)
        input_dict['inst'] = inst_input
        info_dict['inst'] = inst_info


    if 'pos' in input_order:    
        pos_input  = Input(shape=(Tpos,  npos ), name='pos_input')
        pos_info = pos_input
        # pos_info = TimeDistributed( Dense(256, activation='relu', kernel_initializer=ker_init1))(pos_info)
        # pos_info = TimeDistributed( Dense(128, activation='relu', kernel_initializer=ker_init1))(pos_info)
        pos_info = Bidirectional(LSTM(16, return_sequences=False, kernel_initializer=ker_init1))(pos_info)
        input_dict['pos'] = pos_input
        info_dict['pos'] = pos_info
 

    if 'br' in input_order:
        br_input = Input(shape=(Tbr, nbr), name='bure_input')
        br_info = pos_input
        # br_info = TimeDistributed( Dense(256, activation='relu', kernel_initializer=ker_init1))(br_info)
        # br_info = TimeDistributed( Dense(128, activation='relu', kernel_initializer=ker_init1))(br_info)
        br_info = Bidirectional(LSTM(64, return_sequences=False, kernel_initializer=ker_init1))(br_info)
        input_dict['br'] = br_input
        info_dict['br'] = br_info

    info_list = [info_dict[i] for i in input_order]

    x = Concatenate()(info_list)
    x = Dense(1024, activation='relu', kernel_initializer=ker_init1)(x)
    x = Dense(512, activation='relu', kernel_initializer=ker_init1)(x)
    x = Dense(1, activation='sigmoid', kernel_initializer=ker_init1)(x)

    inputs = [input_dict[i] for i in input_order]
    outputs = x

    model = keras.models.Model(inputs=inputs, outputs=outputs)

    return model


def arrangeTrainValSets(app_data, Npos_val, Nneg_val, batch_size=32, seed=0):
    """
    app_data -- a dataframe
    Npos_val -- a number of positive classification for the validation set
    Nneg_val -- a number of negative classification for the validation set
    batch_size -- the batch size
    """
    sub0 = app_data.loc[app_data['TARGET']==0,:].copy(True)
    sub1 = app_data.loc[app_data['TARGET']==1,:].copy(True)

    sub1_val = sub1.sample(Npos_val, random_state=0, axis=0)
    sub1_train = sub1.drop(sub1_val.index, axis=0)

    sub0_val = sub0.sample(Nneg_val, random_state=0, axis=0)
    sub0_train = sub0.drop(sub0_val.index, axis=0)

    N0 = sub0_train.shape[0]
    N1 = sub1_train.shape[0]
    # print('Train 1 num: {}'.format(N1))
    # print('Train 0 num: {}'.format(N0))
    # print('Total: {}'.format(N0+N1))

    rounds = int(np.ceil(float(N0+N1)/batch_size))
    # print('Round: {}'.format(rounds))

    N1in_base = int(np.ceil(float(N1)/N0*batch_size))-1
    # print('N1in_base: {}'.format(N1in_base))
    spill = N1 - N1in_base*rounds
    # print('spill: {}'.format(spill))

    pos_index = sub1_train.sample(n=N1, random_state=seed).index
    neg_index = sub0_train.sample(n=N0, random_state=seed).index

    ids = pd.Index([])
    r = 0
    for r in tqdm(range(0, rounds)):
        if r<spill:
            N1in = N1in_base+1
        else:
            N1in = N1in_base
        N0in = batch_size-N1in
        
        pos = pos_index[0:N1in]
        if pos.size>0:
            ids = ids.append(pos)
            pos_index = pos_index.drop(pos)
            
        neg = neg_index[0:N0in]
        if neg.size>0:
            ids = ids.append(neg)
            neg_index = neg_index.drop(neg)
        
        r += 1

    train_set = sub1_train.append(sub0_train)
    train_set = train_set.loc[ids, :]

    val_set = sub1_val.append(sub0_val)
    val_set = val_set.sample(n=val_set.shape[0], random_state=seed)

    return (train_set, val_set)



class Sequencer(Sequence):

    def __init__(self, datasets, input_order, datastructure=None, batch_size=32, isTestSet=False):
        # check if datasets and input_order match
        for item in input_order:
            if item not in datasets.keys():
                raise ValueError('{} of input_order is not in the datasets.'.format(item))

        if 'bureau_bal_data' in datasets.keys() and 'bureau_data' not in datasets.keys():
            raise ValueError('bureau_bal_data needs bureau_data to work.')

        self.datasets = datasets
        if datastructure is None:
            datastructure = getDataStructure(datasets)
        self.datastructure = datastructure
        self.input_order = input_order
        self.batch_size = batch_size
        self.index = datasets['app'].index
        self.isTestSet = isTestSet

    @property
    def input_order(self):
        return self._input_order

    @input_order.setter
    def input_order(self, value):
        self._input_order = value

    @property
    def size(self):
        return self.__len__()

    @property
    def isTestSet(self):
        return self._isTestSet

    @isTestSet.setter
    def isTestSet(self, value):
        self._isTestSet = value

    def __len__(self):
        return int(np.ceil(self.index.size / float(self.batch_size)))

    def __getitem__(self, idx):

        index = self.index[ idx * self.batch_size : (idx + 1) * self.batch_size]

        result = Sequencer.npBatchMaker(self.datasets, index, self.datastructure, index.size, self.isTestSet)

        xtrains = []
            
        for item in self.input_order:
            xtrains.append(result[item])

        if self.isTestSet:
            return xtrains
        else:
            ytrains = result['target']
            return (xtrains, ytrains)

    @staticmethod
    def npBatchMaker(datasets, index, datastructure, batch_size, isTestSet):
        """
        index -- a pandas' Index
        """

        app_data = datasets['app'].loc[index, :].copy(deep=True)
        app_ids = app_data['SK_ID_CURR'].values
        if isTestSet:
            app_data = app_data.drop(['SK_ID_CURR'])
        else:
            target = app_data['TARGET'].values
            app_data = app_data.drop(['SK_ID_CURR', 'TARGET'], axis=1)

        npdata = dict()
        npdata['app'] = app_data.values

        # prev data
        prev_data = datasets['prev']
        n = datastructure['prev']['n']
        T = datastructure['prev']['T']
        prev_data_batch = np.zeros(shape=(batch_size, T, n))

        # cred data
        cred_data = datasets['cred']
        n = datastructure['cred']['n']
        T = datastructure['cred']['T']
        cred_data_batch = np.zeros(shape=(batch_size, T, n))

        # pos data
        pos_data = datasets['pos']
        n = datastructure['pos']['n']
        T = datastructure['pos']['T']
        pos_data_batch = np.zeros(shape=(batch_size, T, n))

        # inst data
        inst_data = datasets['inst']
        n = datastructure['inst']['n']
        T = datastructure['inst']['T']
        N = datastructure['inst']['N']
        inst_data_batch = np.zeros(shape=(batch_size, T, N, n))

        # bureau
        br_data = datasets['br']
        n = datastructure['br']['n']
        T = datastructure['br']['T']
        br_data_batch = np.zeros(shape=(batch_size, T, n))


        for i, app_id in enumerate(app_ids):
            # prev
            subdata = prev_data.loc[ prev_data['SK_ID_CURR']==app_id, :].drop(['SK_ID_CURR','SK_ID_PREV'], axis=1)
            subdata.sort_values(['DAYS_DECISION'], ascending=False)
            prev_data_batch[i, 0:subdata.shape[0]] = subdata.values

            # cred
            subdata = cred_data.loc[ cred_data['SK_ID_CURR']==app_id, :].drop(['SK_ID_CURR', 'SK_ID_PREV'], axis=1)
            cred_data_batch[i, 0:subdata.shape[0]] = subdata.values

            # pos
            subdata = pos_data.loc[ pos_data['SK_ID_CURR']==app_id, :].drop(['SK_ID_CURR','SK_ID_PREV'], axis=1)
            pos_data_batch[i, 0:subdata.shape[0]] = subdata.values

            # inst
            subdata = inst_data.loc[ inst_data['SK_ID_CURR']==app_id, :].drop(['SK_ID_CURR'], axis=1)
            grouped = subdata.groupby(['SK_ID_PREV'])
            keys = list(grouped.groups.keys())
            for j, key in enumerate(keys):
                subbdata0 = grouped.get_group(key).drop(['SK_ID_PREV'], axis=1)
                inst_data_batch[i, j, 0:subbdata0.shape[0]] = subbdata0.values

            # bureau
            subdata = br_data.loc[ br_data['SK_ID_CURR']==app_id, :].drop(['SK_ID_CURR','SK_ID_BUREAU'], axis=1)
            br_data_batch[i, 0:subdata.shape[0]] = subdata.values


        npdata['prev'] = prev_data_batch
        npdata['cred'] = cred_data_batch
        npdata['pos']  = pos_data_batch
        npdata['inst'] = inst_data_batch
        npdata['br'] = br_data_batch


        if isTestSet==False:
            npdata['target'] = target

        return npdata



def getDataStructure(datasets):
    names = list(datasets.keys())
    datastructure = dict()
    for name in names:
        data = datasets[name]
        dims = dict()
        dims['n'] = data.shape[0]
        if name == 'app':
            dims['n'] = data.shape[1]-2
        if name == 'prev':
            dims['n'] = data.shape[1]-2
            dims['T'] = data.groupby('SK_ID_CURR')['SK_ID_PREV'].count().max()
        if name == 'cred':
            dims['n'] = data.shape[1]-2
            dims['T'] = data.groupby('SK_ID_CURR')['SK_ID_PREV'].count().max()
        if name == 'pos':
            dims['n'] = data.shape[1]-2
            dims['T'] = data.groupby('SK_ID_CURR')['SK_ID_PREV'].count().max()
        if name == 'inst':
            dims['n'] = data.shape[1]-2
            dims['N'] = data.groupby(['SK_ID_CURR','SK_ID_PREV'])['DAYS_INSTALMENT'].count().max()
            dims['T'] = data.groupby('SK_ID_CURR')['SK_ID_PREV'].count().max()
        if name == 'br':
            dims['n'] = data.shape[1]-2
            dims['T'] = data.groupby('SK_ID_CURR')['SK_ID_BUREAU'].count().max()

        datastructure[name] = dims

    return datastructure


def massageDataset(df, followers=[], normalize=True, verbose=True):

    imp_mode = Imputer(strategy='most_frequent', verbose=1)
    imp_mean = Imputer(strategy='mean', verbose=1)
    le = LabelEncoder()

    label_dict = dict()
    norm_dict = dict()

    for col in df:

        ctype = df[col].dtype

        if col in ['SK_ID_CURR','SK_ID_PREV','SK_ID_BUREAU']:
            continue

        if verbose:
            print('{} -- type {}'.format(col, ctype))

        ctype = df[col].dtype
        if ctype == 'object':    
            # replace missing value with mode value
            try:
                replace_val = df[col].mode().values[0]
            except:
                replace_val = 'NAPP'
            df.loc[ df[col].isnull(), col] = replace_val
            
            for d in followers:
                d.loc[ d[col].isnull(), col] = replace_val

            # label encoding
            le.fit(list(df[col]))
            label_dict[col] = list(le.classes_)
            df[col] = le.transform(list(df[col]))
            
            for d in followers:
                d[col] = le.transform(list(d[col]))
            
        elif ctype  == 'int64':
            # replace missing value with mode value
            replace_val = df[col].mode().values[0]
            df.loc[ df[col].isnull(), col] = replace_val

            vals = np.expand_dims(df[col].values, axis=1)
            imp_mode.fit(vals)
            df[col] = imp_mode.transform(vals)
            
            for d in followers:
                d[col] = imp_mode.transform(np.expand_dims(d[col].values, axis=1))
                
        elif ctype == 'float64':
            # replace missing value with mean value
            # replace_val = df[col].mean()
            df.loc[ df[col].isnull(), col] = 0

            # replace inf and -inf value
            df.replace([np.inf, -np.inf], 0)

            if normalize:
                vals = np.expand_dims(df[col].values, axis=-1)
                imp_mean.fit(vals)
                df[col] = imp_mean.transform(vals)

                mean = df[col].mean()
                std = df[col].std()
                df[col] = (df[col]-mean)/std

                norm_dict[col] = (mean, std)
                
                for d in followers:
                    d[col] = imp_mean.transform(np.expand_dims(d[col].values, axis=-1))
                    d[col] = (d[col]-mean)/std


    return (df, followers, label_dict, norm_dict)


def correctApplication(data):

    # 'DAYS_EMPLOYED'. Remove data points that >300,000 (which is equivalent to 1000 years, stupid value)
    col = 'DAYS_EMPLOYED'
    meanval = data.loc[data[col]<300000, col].mean()

    mask = data[col]>=300000
    data['DAYS_EMPLOYED_ANM'] = 0
    data.loc[ mask, 'DAYS_EMPLOYED_ANM'] = 1
    data.loc[ mask , col] = meanval

    return data


def correctPrevApp(data):
    #
    col = 'DAYS_FIRST_DRAWING'
    meanval = data.loc[data[col]<300000, col].mean()
    mask = data[col]>=300000
    data['DAYS_EMPLOYED_ANM'] = 0
    data.loc[ mask, 'DAYS_EMPLOYED_ANM'] = 1
    data.loc[ mask , col] = meanval

    #
    col = 'DAYS_FIRST_DUE'
    meanval = data.loc[data[col]<300000, col].mean()
    mask = data[col]>=300000
    data['DAYS_EMPLOYED_ANM'] = 0
    data.loc[ mask, 'DAYS_EMPLOYED_ANM'] = 1
    data.loc[ mask , col] = meanval

    #
    col = 'DAYS_LAST_DUE_1ST_VERSION'
    meanval = data.loc[data[col]<300000, col].mean()
    mask = data[col]>=300000
    data['DAYS_EMPLOYED_ANM'] = 0
    data.loc[ mask, 'DAYS_EMPLOYED_ANM'] = 1
    data.loc[ mask , col] = meanval

    #
    col = 'DAYS_LAST_DUE'
    meanval = data.loc[data[col]<300000, col].mean()
    mask = data[col]>=300000
    data['DAYS_EMPLOYED_ANM'] = 0
    data.loc[ mask, 'DAYS_EMPLOYED_ANM'] = 1
    data.loc[ mask , col] = meanval

    #
    col = 'DAYS_TERMINATION'
    meanval = data.loc[data[col]<300000, col].mean()
    mask = data[col]>=300000
    data['DAYS_EMPLOYED_ANM'] = 0
    data.loc[ mask, 'DAYS_EMPLOYED_ANM'] = 1
    data.loc[ mask , col] = meanval

    return data


def correctCredBal(df):
    for col in list(df.columns.values):
        if col.find('AMT_')!=0 or col.find('CNT_')!=0:
            df.loc[ df[col].isnull(), col] = 0

    return df


def correctPOS(df):
    for col in list(df.columns.values):
        if col.find('CNT_')!=0:
            df.loc[ df[col].isnull(), col] = 0

    cols = ['SK_DPD', 'SK_DPD_DEF']
    for col in cols:
        mask = df[col]>3650
        df.loc[mask, col] = 3650

    return df


def correctInst(df):

    mask = df['DAYS_ENTRY_PAYMENT'].isnull()

    col = 'PAYMENT_OBSERVED'
    df[col] = 1
    df.loc[mask, col] = 0

    col = 'DAYS_ENTRY_PAYMENT'
    df.loc[mask, col] = 0

    col = 'AMT_PAYMENT'
    df.loc[mask, col] = 0

    df['DAYS_PAY_IN_ADVANCE'] = df['DAYS_INSTALMENT'] - df['DAYS_ENTRY_PAYMENT']
    df['AMT_DIFF'] = df['AMT_PAYMENT'] - df['AMT_INSTALMENT']

    return df


def correctBureau(df):
    col = 'DAYS_CREDIT_ENDDATE'
    mask = df[col].isnull()
    df.loc[mask, col] = 0
    df['DAY_CREDIT_ENDDATE_NA'] = 0
    df.loc[mask, 'DAY_CREDIT_ENDDATE_NA'] = 1

    mask = df[col]<-3650
    df.loc[mask, col] = -3650
    mask = df[col]>3650
    df.loc[mask, col] = 3650

    col = 'DAYS_ENDDATE_FACT'
    mask = df[col]<-3650
    df.loc[mask, col] = -3650
    mask = df[col].isnull()
    df[col+'_NA'] = 0
    df.loc[mask, col+'_NA'] = 1

    col = 'AMT_CREDIT_MAX_OVERDUE'
    mask = df[col]>1e6
    df.loc[mask, col] = 1e6
    mask = df[col].isnull()
    df[col+'_NA'] = 0
    df.loc[mask, col+'_NA'] = 1

    cols = ['AMT_CREDIT_SUM', 'AMT_CREDIT_SUM_DEBT', 'AMT_CREDIT_SUM_LIMIT', 'AMT_CREDIT_SUM_OVERDUE', 'AMT_ANNUITY']
    for col in cols:
        replace_val = df[col].median()
        mask = df[col].isnull()
        df.loc[mask, col] = replace_val

    col = 'DAYS_CREDIT_UPDATE'
    mask = df[col]<-3650
    df.loc[mask, col] = -3650
    mask = df[col]>3650
    df.loc[mask, col] = 3650

    return df


def renameObjectCol(df):
    cols = df.columns.values
    for col in cols:
        ctype = df[col].dtype
        if ctype == 'object':
            df.rename({col: 'CLASS_'+col}, axis='columns', inplace=True)

    return df





