?	AMT_APPLICATION always = AMT_GOODS_PRICE
?	AMT_DOWN_PAYMENT = RATE_DOWN_PAYMENT
?	CODE_REJECT_REASON would be XAP if NAME_CONTRACT_STATUS is Approved
?	Connection exists between 'NAME_SELLER_INDUSTRY' and 'NAME_GOODS_CATEGORY'
?	'NAME_CONTRACT_TYPE' have the same unique values as in the application file


Observation:

'NAME_CONTRACT_STATUS'
	- can safely throw away 'Canceled' data
	- if it is 'Unused offer', 'CODE_REJECT_REASON' is typically 'CLIENT'. It could mean that the client declined to sign the contract.

'DAYS_DECISION'
	- good to rank loans in time

'NAME_SELLER_INDUSTRY'
	- Should investigate whether a particular goods type has a higher chance of default or not.

'SK_ID_CURR'
	- One unique SK_ID_CURR can have many SK_ID_PREV


select_cols = ['SK_ID_PREV', 'SK_ID_CURR', 'NAME_CONTRACT_TYPE', 'AMT_ANNUITY', 'AMT_APPLICATION',
	'AMT_CREDIT', 'AMT_DOWN_PAYMENT', 'AMT_GOODS_PRICE', 'NAME_CONTRACT_STATUS',
	'DAYS_DECISION', 'NAME_PAYMENT_TYPE', 'CODE_REJECT_REASON', 'NAME_TYPE_SUITE',
	'NAME_CLIENT_TYPE', 'NAME_GOODS_CATEGORY', 'NAME_PORTFOLIO', 'NAME_SELLER_INDUSTRY',
	'CNT_PAYMENT', 'NAME_YIELD_GROUP', 'NFLAG_INSURED_ON_APPROVAL']


***************
- The decision of how to handle missing values can be re-defined later, after I establish the model.



=========================================================================================================
CREDIT_BALANCE data sheet

- I should focus on rows with similar previous and current ids
- Each current id can have multiple previous ids.
- CREDIT_BALANCE can be listed in sequence using MONTHS_BALANCE

It's better to pick previous and current ids from PREV_APP_DATA rather than from CREDIT_BALANCE data

It is possible that for a pair of current id and previous id, there is NO DATA in the CREDIT_BALANCE. Found a casew when there is not current id (271877) in credit balance data sheet.

Num uniq id in prev_data: 338402
Num uniq id in cred_bal:  103558
Num of overlap ids:       102588

This means there the set of current ids from prev_app_data and credit_balance are two overlapping sets in a Venn diagram.

Does it mean I have to combine this credit balance with the one from the bureau?

There is a case when there current id from prev_data can be found, but the previous_id cannot be matched.
Is this because the application is rejected?



=========================================================================================================
INST_PAYMENT data sheet

Num uniq id in prev_data: 338402
Num uniq id in inst_pay:  339587
Num of overlap ids:       337574

Same situation happens about overlapping current ids. The number of missing current is not that bad compared to the credit_balance.
