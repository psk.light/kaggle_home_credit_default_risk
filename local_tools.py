import numpy as np
import pandas as pd
import keras
import keras.backend as K
from keras.utils import Sequence
import tensorflow as tf
import MLme as tools

class Sequencer(Sequence):

    def __init__(self, datasets, caches, datastructure, input_order, batch_size=32, isTestSet=False):
        # check if datasets and input_order match
        for item in input_order:
            if item not in datasets.keys():
                raise ValueError('{} of input_order is not in the datasets.'.format(item))

        if 'bureau_bal_data' in datasets.keys() and 'bureau_data' not in datasets.keys():
            raise ValueError('bureau_bal_data needs bureau_data to work.')

        self.datasets = datasets
        self.caches = caches
        self.datastructure = datastructure
        self.input_order = input_order
        self.batch_size = batch_size
        self.index = datasets['app_data'].index
        self.isTestSet = isTestSet



    @property
    def input_order(self):
        return self._input_order

    @input_order.setter
    def input_order(self, value):
        self._input_order = value

    @property
    def size(self):
        return self.__len__()

    @property
    def isTestSet(self):
        return self._isTestSet

    @isTestSet.setter
    def isTestSet(self, value):
        self._isTestSet = value

    def __len__(self):
        return int(np.ceil(self.index.size / float(self.batch_size)))

    def __getitem__(self, idx):

        index = self.index[ idx * self.batch_size : (idx + 1) * self.batch_size]

        result = Sequencer.npBatchMaker(self.datasets, self.caches, 
            self.datastructure, index, index.size, self.isTestSet) #, self.neglectBureauBal)

        xtrains = []
            
        for item in self.input_order:
            xtrains.append(result[item])
            

        if self.isTestSet:
            return xtrains
        else:
            ytrains = result['y_train']
            return (xtrains, ytrains)


    @staticmethod
    def npBatchMaker(datasets, caches, datastructure, index, batch_size, isTestSet):
        """
        index -- a pandas' Index
        """

        npdata = dict()


        if 'app_data' in datasets.keys():
            app_data = datasets['app_data']
            app_data_onehot_dict = caches['onehots']['app_data']
            app_data_norm_factors = caches['norm_factors']['app_data']

            app_data_sub = app_data.loc[index,:]
            ids_curr = app_data_sub['SK_ID_CURR'].unique()

            app_data_batch_all = convertDFtoNP(app_data_sub, ohdict=app_data_onehot_dict, normdict=app_data_norm_factors)

            if isTestSet:
                app_data_batch = app_data_batch_all[:, 1:]
            else:
                app_data_batch = app_data_batch_all[:, 2:]
                y_train_batch = app_data_batch_all[:, 1]

            npdata['app_data'] = app_data_batch

            if isTestSet==False:
                npdata['y_train'] = y_train_batch


        if 'prev_app_data' in datasets.keys():
            prev_app_data = datasets['prev_app_data']
            Tpr = datastructure['Tpr']
            npr = datastructure['npr']
            prev_data_onehot_dict = caches['onehots']['prev_app_data']
            prev_data_norm_factors = caches['norm_factors']['prev_app_data']

            prev_app_data_batch = np.zeros((batch_size, Tpr, npr))


        if 'cred_bal_data' in datasets.keys():
            cred_bal_data = datasets['cred_bal_data']
            Tcred = datastructure['Tcred']
            ncred = datastructure['ncred']
            Nprev_cred = datastructure['Nprev_cred']
            cred_data_onehot_dict = caches['onehots']['cred_bal_data']
            cred_data_norm_factors = caches['norm_factors']['cred_bal_data']

            cred_bal_data_batch = np.zeros((batch_size, Nprev_cred, Tcred, ncred))


        if 'inst_pay_data' in datasets.keys():
            inst_pay_data = datasets['inst_pay_data']
            Tinst = datastructure['Tinst']
            ninst = datastructure['ninst']
            Nprev_inst = datastructure['Nprev_inst']
            inst_data_onehot_dict = caches['onehots']['inst_pay_data']
            inst_data_norm_factors = caches['norm_factors']['inst_pay_data']

            inst_pay_data_batch = np.zeros((batch_size, Nprev_inst, Tinst, ninst))


        if 'pos_cash_data' in datasets.keys():
            pos_cash_data = datasets['pos_cash_data']
            Tpos = datastructure['Tpos']
            npos = datastructure['npos']
            Nprev_pos = datastructure['Nprev_pos']
            pos_data_onehot_dict = caches['onehots']['pos_cash_data']
            pos_data_norm_factors = caches['norm_factors']['pos_cash_data']

            pos_cash_data_batch = np.zeros((batch_size, Nprev_pos, Tpos, npos))


        if 'bureau_data' in datasets.keys():
            bureau_data   = datasets['bureau_data']
            Nprev_br  = datastructure['Nprev_br']
            nbr = datastructure['nbr']
            bureau_onehot_dict = caches['onehots']['bureau_data']
            bureau_norm_factors = caches['norm_factors']['bureau_data']

            bureau_data_batch = np.zeros((batch_size, Nprev_br, nbr))


        if 'bureau_bal_data' in datasets.keys():
            bureau_bal_data = datasets['bureau_bal_data']
            Nprev_brb = datastructure['Nprev_brb']
            Tbrb = datastructure['Tbrb']
            nbrb = datastructure['nbrb']
            bureau_bal_onehot_dict = caches['onehots']['bureau_bal_data']
            bureau_bal_norm_factors = caches['onehots']['bureau_bal_data']

            bureau_bal_data_batch = np.zeros((batch_size, Nprev_brb, Tbrb, nbrb ))




        if 'prev_app_data' in datasets.keys():
            idcurr_i_dict = {y: x for x, y in enumerate(ids_curr)}
            prev_app_data_sub = prev_app_data.loc[ prev_app_data['SK_ID_CURR'].isin(ids_curr), :].sort_values(['SK_ID_CURR', 'DAYS_DECISION'], ascending=True)
            grouped = prev_app_data_sub.groupby(['SK_ID_CURR'])
            groupnames = grouped.groups

            for name, ind in groupnames.items():
                subdata = grouped.get_group(name)
                if subdata.shape[0]>0:
                    subdata_np = convertDFtoNP(subdata, prev_data_onehot_dict, prev_data_norm_factors)
                    t = subdata_np.shape[0]
                    if t>Tpr:
                        t = Tpr
                    prev_app_data_batch[idcurr_i_dict[name], 0:t] = subdata_np[0:t, 2:]

            npdata['prev_app_data'] = prev_app_data_batch


        if 'cred_bal_data' in datasets.keys():
            cred_bal_data_sub = cred_bal_data.loc[ cred_bal_data['SK_ID_CURR'].isin(ids_curr), :].sort_values(['SK_ID_CURR', 'SK_ID_PREV'], ascending=True)
            grouped = cred_bal_data_sub.groupby(['SK_ID_CURR','SK_ID_PREV'])

            Nbin = {idc: 0 for idc in ids_curr}
            for name, ind in grouped.groups.items():
                subdata = grouped.get_group(name)
                if subdata.shape[0]>0:
                    subdata_np = convertDFtoNP(subdata, cred_data_onehot_dict, cred_data_norm_factors)
                    t = subdata_np.shape[0]
                    if t>Tcred:
                        t = Tcred
                    if Nbin[name[0]]<=Nprev_cred:
                        cred_bal_data_batch[idcurr_i_dict[name[0]], Nbin[name[0]], 0:t] = subdata_np[0:t, 2:]
                        Nbin[name[0]] += 1

            npdata['cred_bal_data'] = cred_bal_data_batch


        if 'inst_pay_data' in datasets.keys():
            inst_pay_data_sub = inst_pay_data.loc[ inst_pay_data['SK_ID_CURR'].isin(ids_curr), :].sort_values(['SK_ID_CURR', 'NUM_INSTALMENT_NUMBER'], ascending=True)
            grouped = inst_pay_data_sub.groupby(['SK_ID_CURR', 'SK_ID_PREV'])

            Nbin = {idc: 0 for idc in ids_curr}
            for name, ind in grouped.groups.items():
                subdata = grouped.get_group(name)
                if subdata.shape[0]>0:
                    subdata_np = convertDFtoNP(subdata, inst_data_onehot_dict, inst_data_norm_factors)
                    t = subdata_np.shape[0]
                    if t>Tinst:
                        t = Tinst
                    if Nbin[name[0]]<=Nprev_inst:
                        inst_pay_data_batch[idcurr_i_dict[name[0]], Nbin[name[0]], 0:t] = subdata_np[0:t, 2:]
                        Nbin[name[0]] += 1

            npdata['inst_pay_data'] = inst_pay_data_batch


        if 'pos_cash_data' in datasets.keys():
            pos_cash_data_sub = pos_cash_data.loc[ pos_cash_data['SK_ID_CURR'].isin(ids_curr), :].sort_values(['SK_ID_CURR', 'MONTHS_BALANCE'], ascending=True)
            grouped = inst_pay_data_sub.groupby(['SK_ID_CURR', 'SK_ID_PREV'])

            Nbin = {idc: 0 for idc in ids_curr}
            for name, ind in grouped.groups.items():
                subdata = grouped.get_group(name)
                if subdata.shape[0]>0:
                    subdata_np = convertDFtoNP(subdata, pos_data_onehot_dict, pos_data_norm_factors)
                    t = subdata_np.shape[0]
                    if t>Tpos:
                        t = Tpos
                    if Nbin[name[0]]<=Nprev_pos:
                        pos_cash_data_batch[idcurr_i_dict[name[0]], Nbin[name[0]], 0:t] = subdata_np[0:t, 2:]
                        Nbin[name[0]] += 1

            npdata['pos_cash_data'] = pos_cash_data_batch


        if 'bureau_data' in datasets.keys():
            bureau_data_sub = bureau_data.loc[ bureau_data['SK_ID_CURR'].isin(ids_curr), :].sort_values(['SK_ID_CURR', 'DAYS_CREDIT'], ascending=True)
            uniq_bureau_ids = bureau_data_sub['SK_ID_BUREAU'].unique()
            grouped = bureau_data_sub.groupby(['SK_ID_CURR'])

            for name, ind in grouped.groups.items():
                subdata = grouped.get_group(name)
                if subdata.shape[0]>0:
                    subdata_np = convertDFtoNP(subdata, bureau_onehot_dict, bureau_norm_factors)
                    t = subdata_np.shape[0]
                    if t>Nprev_br:
                        t = Nprev_br
                    bureau_data_batch[idcurr_i_dict[name], 0:t] = subdata_np[0:t, 2:]

            npdata['bureau_data'] = bureau_data_batch

            bureau_hc_dict = dict()
            grouped = bureau_data_sub.groupby(['SK_ID_CURR','SK_ID_BUREAU'])
            for key in grouped.groups.keys():
                bureau_hc_dict[key[1]] = key[0]

        

        if 'bureau_data' in datasets.keys() and 'bureau_bal_data' in datasets.keys():
            bureau_bal_data_sub = bureau_bal_data[ bureau_bal_data['SK_ID_BUREAU'].isin(uniq_bureau_ids)].sort_values(['SK_ID_BUREAU'], ascending=True)
            grouped = bureau_bal_data_sub.groupby(['SK_ID_BUREAU'])

            Nbin = {idc: 0 for idc in uniq_bureau_ids}
            
            for name, ind in grouped.groups.items():
                # print('{}, {}'.format(name, ind))
                subdata = grouped.get_group(name)
                if subdata.shape[0]>0:
                    subdata_np = convertDFtoNP(subdata, bureau_bal_onehot_dict, bureau_bal_norm_factors)
                    # print(subdata_np.shape)
                    t = subdata_np.shape[0]
                    if t>Tbrb:
                        t = Tbrb
                    if Nbin[name]<=Nprev_brb:
                        bureau_bal_data_batch[idcurr_i_dict[bureau_hc_dict[name]], Nbin[name], 0:t] = subdata_np[0:t, 1:]
                        Nbin[name] += 1

            npdata['bureau_bal_data'] = bureau_bal_data_batch


        return npdata


def getPrediction(model, datasets, caches, datastructure, input_order, batch_size, steps=None):
    file_app_test = './data/application_test.csv'
    app_test_data = pd.read_csv(file_app_test)
    app_test_data.index = range(307511, 307511+app_test_data.shape[0])
    select_cols = ['SK_ID_CURR', 'NAME_CONTRACT_TYPE', 'CODE_GENDER',
        'FLAG_OWN_CAR', 'FLAG_OWN_REALTY', 'CNT_CHILDREN',
        'AMT_INCOME_TOTAL', 'AMT_CREDIT', 'AMT_ANNUITY', 'AMT_GOODS_PRICE',
        'NAME_INCOME_TYPE', 'NAME_EDUCATION_TYPE', 'NAME_FAMILY_STATUS',
        'NAME_HOUSING_TYPE', 'DAYS_BIRTH', 'DAYS_EMPLOYED', 'OCCUPATION_TYPE']

    dummy = prepareTestSet(app_test_data, caches['labels']['app_data'])
    datasets['app_data'] = dummy[0]
    caches['labels']['app_data'] = dummy[1]
    caches['onehots']['app_data'] = dummy[2]

    seq1 = Sequencer(datasets, caches, datastructure, input_order, batch_size, isTestSet=True)

    pred = model.predict_generator(seq1, verbose=1, steps=steps)

    ids = app_test_data['SK_ID_CURR'].values[0:pred.size]

    return (pred.reshape((pred.size,)), ids)


def saveToSubmission(prediction, filename):

    result = np.vstack((prediction[1], prediction[0]))

    result = pd.DataFrame(result.transpose(), columns=['SK_ID_CURR', 'TARGET'])

    result['SK_ID_CURR'] = result['SK_ID_CURR'].astype('int32', copy=False)

    result.to_csv(filename,index=False)


def buildModel_v5c(datastructure):
    Input = keras.layers.Input
    LSTM = keras.layers.LSTM
    TimeDistributed = keras.layers.TimeDistributed
    Dense = keras.layers.Dense
    Model = keras.models.Model
    Concatenate = keras.layers.Concatenate
    Dropout = keras.layers.Dropout

    napp = datastructure['napp']
    Tpr = datastructure['Tpr']
    npr = datastructure['npr']
    Tcred = datastructure['Tcred']
    ncred = datastructure['ncred']
    Nprev_cred = datastructure['Nprev_cred']
    # Tinst = datastructure['Tinst']
    # ninst = datastructure['ninst']
    # Nprev_inst = datastructure['Nprev_inst']
    # Tpos = datastructure['Tpos']
    # npos = datastructure['npos']
    # Nprev_pos = datastructure['Nprev_pos']
    Nprev_br  = datastructure['Nprev_br']
    nbr = datastructure['nbr']
    # Nprev_brb = datastructure['Nprev_brb']
    # Tbrb = datastructure['Tbrb']
    # nbrb = datastructure['nbrb']

    app_input = Input(shape=(napp,), name='app_input')
    prev_input = Input(shape=(Tpr, npr), name='prev_input')
    cred_input = Input(shape=(Nprev_cred, Tcred, ncred), name='cred_input')
    # inst_input = Input(shape=(Nprev_inst, Tinst, ninst), name='inst_input')
    # pos_input  = Input(shape=(Nprev_pos,  Tpos,  npos ), name='pos_input')
    bure_input = Input(shape=(Nprev_br, nbr), name='bure_input')

    ker_init1 = keras.initializers.RandomNormal(mean=0.0, stddev=0.3, seed=0)

    # if neglectBureauBal==False:
    #     bure_bal_input = Input(shape=(Nprev_brb, Tbrb, nbrb), name='bure_bal_input')


    app_info = Dense(128, kernel_initializer=ker_init1, activation='relu', name='dense_app_1')(app_input)
    app_info = Dense(128, kernel_initializer=ker_init1, activation='relu', name='dense_app_2')(app_input)
    app_info = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_app_3')(app_info)
    app_info = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_app_4')(app_info)


    prev_info = LSTM(128, kernel_initializer=ker_init1, name='lstm_prev')(prev_input)
    # prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(128, activation='relu', kernel_initializer=ker_init1, name='dense_prev_1')(prev_info)
    # prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_2')(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_3')(prev_info)

    
    cred_info = TimeDistributed(
                        LSTM(64, return_sequences=True, kernel_initializer=ker_init1, name='lstm_cred_1')
                , name='timedist_cred')(cred_input)
    cred_info = TimeDistributed(LSTM(64, kernel_initializer=ker_init1, name='lstm_cred_2'))(cred_info)
    cred_info = LSTM(64, kernel_initializer=ker_init1, name='lstm_cred_3')(cred_info)
    # cred_info = Dropout(0.5)(cred_info)
    cred_info = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_cred_1')(cred_info)
    cred_info = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_cred_2')(cred_info)
    # cred_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_cred_3')(cred_info)

    
    # inst_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1, name='lstm_inst_1'), name='timedist_inst')(inst_input)
    # inst_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_2'))(inst_info)
    # inst_info = LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_3')(inst_info)
    # inst_info = Dropout(0.5)(inst_info)
    # inst_info = Dense(16, kernel_initializer=ker_init1, activation='relu', name='dense_inst_1')(inst_info)


    # pos_info = TimeDistributed(LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_pos_1'), name='timedist_pos')(pos_input)
    # pos_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_2'))(pos_info)
    # pos_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_3')(pos_info)
    # pos_info = Dropout(0.5)(pos_info)
    # pos_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_pos_1')(pos_info)



    bureau = LSTM(64, return_sequences=True, kernel_initializer=ker_init1, name='lstm_bure_0')(bure_input)
    bureau = LSTM(64, kernel_initializer=ker_init1, name='lstm_bure_1')(bureau)
    # bureau = Dropout(0.5)(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_1')(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_2')(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_3')(bureau)

    # if neglectBureauBal==False:
    #     bure_bal_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1,
    #                      name='lstm_bure_bal_1'), name='timedist_brbal')(bure_bal_input)
    #     bure_bal_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_bure_bal_2'))(bure_bal_info)
    #     bure_bal_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_bure_bal_3')(bure_bal_info)
    #     bure_bal_info = Dropout(0.5)(bure_bal_info)
    #     bure_bal_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_bure_bal_1')(bure_bal_info)

    #     bureau = Concatenate()([bureau, bure_bal_info])


    # x = Concatenate()([app_input, prev_info, cred_info, inst_info, pos_info, bureau])
    x = Concatenate()([app_info, prev_info, cred_info, bureau])
    x = Dense(1024, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0a')(x)
    x = Dense(512, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0b')(x)
    x = Dense(512, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0c')(x)
    # x = Dropout(0.5)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1a')(x)
    # x = Dropout(0.9)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1b')(x)
    # x = Dropout(0.8)(x)
    # x = Dense(128, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_2')(x)
    # x = Dropout(0.5)(x)
    # x = Dense(64, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_3')(x)
    # x = Dense(32, activation='relu',  kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_4')(x)
    # x = Dropout(0.5)(x)
    x = Dense(1, activation='sigmoid',kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_5')(x)

    # if neglectBureauBal==False:
    #     inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input, bure_bal_input]
    # else:
    inputs = [app_input, prev_input, cred_input, bure_input]
    model = keras.models.Model(inputs=inputs, outputs=x)

    return model



def buildModel_v5b(datastructure):
    Input = keras.layers.Input
    LSTM = keras.layers.LSTM
    TimeDistributed = keras.layers.TimeDistributed
    Dense = keras.layers.Dense
    Model = keras.models.Model
    Concatenate = keras.layers.Concatenate
    Dropout = keras.layers.Dropout

    napp = datastructure['napp']
    Tpr = datastructure['Tpr']
    npr = datastructure['npr']
    Tcred = datastructure['Tcred']
    ncred = datastructure['ncred']
    Nprev_cred = datastructure['Nprev_cred']
    # Tinst = datastructure['Tinst']
    # ninst = datastructure['ninst']
    # Nprev_inst = datastructure['Nprev_inst']
    # Tpos = datastructure['Tpos']
    # npos = datastructure['npos']
    # Nprev_pos = datastructure['Nprev_pos']
    Nprev_br  = datastructure['Nprev_br']
    nbr = datastructure['nbr']
    # Nprev_brb = datastructure['Nprev_brb']
    # Tbrb = datastructure['Tbrb']
    # nbrb = datastructure['nbrb']

    app_input = Input(shape=(napp,), name='app_input')
    prev_input = Input(shape=(Tpr, npr), name='prev_input')
    cred_input = Input(shape=(Nprev_cred, Tcred, ncred), name='cred_input')
    # inst_input = Input(shape=(Nprev_inst, Tinst, ninst), name='inst_input')
    # pos_input  = Input(shape=(Nprev_pos,  Tpos,  npos ), name='pos_input')
    bure_input = Input(shape=(Nprev_br, nbr), name='bure_input')

    ker_init1 = keras.initializers.RandomNormal(mean=0.0, stddev=0.1, seed=0)

    # if neglectBureauBal==False:
    #     bure_bal_input = Input(shape=(Nprev_brb, Tbrb, nbrb), name='bure_bal_input')


    app_info = Dense(128, kernel_initializer=ker_init1, activation='relu', name='dense_app_1')(app_input)
    app_info = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_app_2')(app_info)
    app_info = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_app_3')(app_info)


    prev_info = LSTM(128, kernel_initializer=ker_init1, name='lstm_prev')(prev_input)
    # prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(128, activation='relu', kernel_initializer=ker_init1, name='dense_prev_1')(prev_info)
    # prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_2')(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_3')(prev_info)

    
    cred_info = TimeDistributed(
                        LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_cred_1')
                , name='timedist_cred')(cred_input)
    cred_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_2'))(cred_info)
    cred_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_3')(cred_info)
    # cred_info = Dropout(0.5)(cred_info)
    cred_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_cred_1')(cred_info)
    cred_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_cred_2')(cred_info)
    cred_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_cred_3')(cred_info)

    
    # inst_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1, name='lstm_inst_1'), name='timedist_inst')(inst_input)
    # inst_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_2'))(inst_info)
    # inst_info = LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_3')(inst_info)
    # inst_info = Dropout(0.5)(inst_info)
    # inst_info = Dense(16, kernel_initializer=ker_init1, activation='relu', name='dense_inst_1')(inst_info)


    # pos_info = TimeDistributed(LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_pos_1'), name='timedist_pos')(pos_input)
    # pos_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_2'))(pos_info)
    # pos_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_3')(pos_info)
    # pos_info = Dropout(0.5)(pos_info)
    # pos_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_pos_1')(pos_info)



    bureau = LSTM(64, return_sequences=True, kernel_initializer=ker_init1, name='lstm_bure_0')(bure_input)
    bureau = LSTM(64, kernel_initializer=ker_init1, name='lstm_bure_1')(bureau)
    # bureau = Dropout(0.5)(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_1')(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_2')(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_3')(bureau)

    # if neglectBureauBal==False:
    #     bure_bal_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1,
    #                      name='lstm_bure_bal_1'), name='timedist_brbal')(bure_bal_input)
    #     bure_bal_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_bure_bal_2'))(bure_bal_info)
    #     bure_bal_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_bure_bal_3')(bure_bal_info)
    #     bure_bal_info = Dropout(0.5)(bure_bal_info)
    #     bure_bal_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_bure_bal_1')(bure_bal_info)

    #     bureau = Concatenate()([bureau, bure_bal_info])


    # x = Concatenate()([app_input, prev_info, cred_info, inst_info, pos_info, bureau])
    x = Concatenate()([app_info, prev_info, cred_info, bureau])
    x = Dense(512, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0a')(x)
    x = Dense(512, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0b')(x)
    # x = Dropout(0.5)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1a')(x)
    # x = Dropout(0.9)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1b')(x)
    # x = Dropout(0.8)(x)
    # x = Dense(128, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_2')(x)
    # x = Dropout(0.5)(x)
    # x = Dense(64, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_3')(x)
    # x = Dense(32, activation='relu',  kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_4')(x)
    # x = Dropout(0.5)(x)
    x = Dense(1, activation='sigmoid',kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_5')(x)

    # if neglectBureauBal==False:
    #     inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input, bure_bal_input]
    # else:
    inputs = [app_input, prev_input, cred_input, bure_input]
    model = keras.models.Model(inputs=inputs, outputs=x)

    return model


def buildModel_v5a(datastructure):
    Input = keras.layers.Input
    LSTM = keras.layers.LSTM
    TimeDistributed = keras.layers.TimeDistributed
    Dense = keras.layers.Dense
    Model = keras.models.Model
    Concatenate = keras.layers.Concatenate
    Dropout = keras.layers.Dropout

    napp = datastructure['napp']
    Tpr = datastructure['Tpr']
    npr = datastructure['npr']
    Tcred = datastructure['Tcred']
    ncred = datastructure['ncred']
    Nprev_cred = datastructure['Nprev_cred']
    # Tinst = datastructure['Tinst']
    # ninst = datastructure['ninst']
    # Nprev_inst = datastructure['Nprev_inst']
    # Tpos = datastructure['Tpos']
    # npos = datastructure['npos']
    # Nprev_pos = datastructure['Nprev_pos']
    Nprev_br  = datastructure['Nprev_br']
    nbr = datastructure['nbr']
    # Nprev_brb = datastructure['Nprev_brb']
    # Tbrb = datastructure['Tbrb']
    # nbrb = datastructure['nbrb']

    app_input = Input(shape=(napp,), name='app_input')
    prev_input = Input(shape=(Tpr, npr), name='prev_input')
    cred_input = Input(shape=(Nprev_cred, Tcred, ncred), name='cred_input')
    # inst_input = Input(shape=(Nprev_inst, Tinst, ninst), name='inst_input')
    # pos_input  = Input(shape=(Nprev_pos,  Tpos,  npos ), name='pos_input')
    bure_input = Input(shape=(Nprev_br, nbr), name='bure_input')

    ker_init1 = keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0)

    # if neglectBureauBal==False:
    #     bure_bal_input = Input(shape=(Nprev_brb, Tbrb, nbrb), name='bure_bal_input')


    app_info = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_app_1')(app_input)
    app_info = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_app_2')(app_info)


    prev_info = LSTM(128, kernel_initializer=ker_init1, name='lstm_prev')(prev_input)
    # prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_1')(prev_info)
    # prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_2')(prev_info)

    
    cred_info = TimeDistributed(
                        LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_cred_1')
                , name='timedist_cred')(cred_input)
    cred_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_2'))(cred_info)
    cred_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_3')(cred_info)
    # cred_info = Dropout(0.5)(cred_info)
    cred_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_cred_1')(cred_info)
    cred_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_cred_2')(cred_info)

    
    # inst_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1, name='lstm_inst_1'), name='timedist_inst')(inst_input)
    # inst_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_2'))(inst_info)
    # inst_info = LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_3')(inst_info)
    # inst_info = Dropout(0.5)(inst_info)
    # inst_info = Dense(16, kernel_initializer=ker_init1, activation='relu', name='dense_inst_1')(inst_info)


    # pos_info = TimeDistributed(LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_pos_1'), name='timedist_pos')(pos_input)
    # pos_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_2'))(pos_info)
    # pos_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_3')(pos_info)
    # pos_info = Dropout(0.5)(pos_info)
    # pos_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_pos_1')(pos_info)



    bureau = LSTM(64, return_sequences=True, kernel_initializer=ker_init1, name='lstm_bure_0')(bure_input)
    bureau = LSTM(64, kernel_initializer=ker_init1, name='lstm_bure_1')(bureau)
    # bureau = Dropout(0.5)(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_1')(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_2')(bureau)

    # if neglectBureauBal==False:
    #     bure_bal_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1,
    #                      name='lstm_bure_bal_1'), name='timedist_brbal')(bure_bal_input)
    #     bure_bal_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_bure_bal_2'))(bure_bal_info)
    #     bure_bal_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_bure_bal_3')(bure_bal_info)
    #     bure_bal_info = Dropout(0.5)(bure_bal_info)
    #     bure_bal_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_bure_bal_1')(bure_bal_info)

    #     bureau = Concatenate()([bureau, bure_bal_info])


    # x = Concatenate()([app_input, prev_info, cred_info, inst_info, pos_info, bureau])
    x = Concatenate()([app_info, prev_info, cred_info, bureau])
    x = Dense(512, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0')(x)
    # x = Dropout(0.5)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1a')(x)
    # x = Dropout(0.9)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1b')(x)
    # x = Dropout(0.8)(x)
    # x = Dense(128, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_2')(x)
    # x = Dropout(0.5)(x)
    # x = Dense(64, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_3')(x)
    # x = Dense(32, activation='relu',  kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_4')(x)
    # x = Dropout(0.5)(x)
    x = Dense(1, activation='sigmoid',kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_5')(x)

    # if neglectBureauBal==False:
    #     inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input, bure_bal_input]
    # else:
    inputs = [app_input, prev_input, cred_input, bure_input]
    model = keras.models.Model(inputs=inputs, outputs=x)

    return model


def buildModel_v4c(datastructure):
    Input = keras.layers.Input
    LSTM = keras.layers.LSTM
    TimeDistributed = keras.layers.TimeDistributed
    Dense = keras.layers.Dense
    Model = keras.models.Model
    Concatenate = keras.layers.Concatenate
    Dropout = keras.layers.Dropout

    napp = datastructure['napp']
    Tpr = datastructure['Tpr']
    npr = datastructure['npr']
    Tcred = datastructure['Tcred']
    ncred = datastructure['ncred']
    Nprev_cred = datastructure['Nprev_cred']
    # Tinst = datastructure['Tinst']
    # ninst = datastructure['ninst']
    # Nprev_inst = datastructure['Nprev_inst']
    # Tpos = datastructure['Tpos']
    # npos = datastructure['npos']
    # Nprev_pos = datastructure['Nprev_pos']
    Nprev_br  = datastructure['Nprev_br']
    nbr = datastructure['nbr']
    # Nprev_brb = datastructure['Nprev_brb']
    # Tbrb = datastructure['Tbrb']
    # nbrb = datastructure['nbrb']

    app_input = Input(shape=(napp,), name='app_input')
    prev_input = Input(shape=(Tpr, npr), name='prev_input')
    cred_input = Input(shape=(Nprev_cred, Tcred, ncred), name='cred_input')
    # inst_input = Input(shape=(Nprev_inst, Tinst, ninst), name='inst_input')
    # pos_input  = Input(shape=(Nprev_pos,  Tpos,  npos ), name='pos_input')
    bure_input = Input(shape=(Nprev_br, nbr), name='bure_input')

    ker_init1 = keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0)

    # if neglectBureauBal==False:
    #     bure_bal_input = Input(shape=(Nprev_brb, Tbrb, nbrb), name='bure_bal_input')

    prev_info = LSTM(128, kernel_initializer=ker_init1, name='lstm_prev')(prev_input)
    # prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_1')(prev_info)
    # prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_2')(prev_info)

    
    cred_info = TimeDistributed(
                        LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_cred_1')
                , name='timedist_cred')(cred_input)
    cred_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_2'))(cred_info)
    cred_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_3')(cred_info)
    # cred_info = Dropout(0.5)(cred_info)
    cred_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_cred_1')(cred_info)

    
    # inst_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1, name='lstm_inst_1'), name='timedist_inst')(inst_input)
    # inst_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_2'))(inst_info)
    # inst_info = LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_3')(inst_info)
    # inst_info = Dropout(0.5)(inst_info)
    # inst_info = Dense(16, kernel_initializer=ker_init1, activation='relu', name='dense_inst_1')(inst_info)


    # pos_info = TimeDistributed(LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_pos_1'), name='timedist_pos')(pos_input)
    # pos_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_2'))(pos_info)
    # pos_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_3')(pos_info)
    # pos_info = Dropout(0.5)(pos_info)
    # pos_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_pos_1')(pos_info)



    bureau = LSTM(64, return_sequences=True, kernel_initializer=ker_init1, name='lstm_bure_0')(bure_input)
    bureau = LSTM(64, kernel_initializer=ker_init1, name='lstm_bure_1')(bureau)
    # bureau = Dropout(0.5)(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_1')(bureau)

    # if neglectBureauBal==False:
    #     bure_bal_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1,
    #                      name='lstm_bure_bal_1'), name='timedist_brbal')(bure_bal_input)
    #     bure_bal_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_bure_bal_2'))(bure_bal_info)
    #     bure_bal_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_bure_bal_3')(bure_bal_info)
    #     bure_bal_info = Dropout(0.5)(bure_bal_info)
    #     bure_bal_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_bure_bal_1')(bure_bal_info)

    #     bureau = Concatenate()([bureau, bure_bal_info])


    # x = Concatenate()([app_input, prev_info, cred_info, inst_info, pos_info, bureau])
    x = Concatenate()([app_input, prev_info, cred_info, bureau])
    x = Dense(512, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0')(x)
    # x = Dropout(0.5)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1')(x)
    # x = Dropout(0.5)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_2')(x)
    x = Dense(1, activation='sigmoid',kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),  name='dense_3')(x)

    # if neglectBureauBal==False:
    #     inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input, bure_bal_input]
    # else:
    inputs = [app_input, prev_input, cred_input, bure_input]
    model = keras.models.Model(inputs=inputs, outputs=x)

    return model


def buildModel_v4b(datastructure):
    Input = keras.layers.Input
    LSTM = keras.layers.LSTM
    TimeDistributed = keras.layers.TimeDistributed
    Dense = keras.layers.Dense
    Model = keras.models.Model
    Concatenate = keras.layers.Concatenate
    Dropout = keras.layers.Dropout

    napp = datastructure['napp']
    Tpr = datastructure['Tpr']
    npr = datastructure['npr']
    Tcred = datastructure['Tcred']
    ncred = datastructure['ncred']
    Nprev_cred = datastructure['Nprev_cred']
    # Tinst = datastructure['Tinst']
    # ninst = datastructure['ninst']
    # Nprev_inst = datastructure['Nprev_inst']
    # Tpos = datastructure['Tpos']
    # npos = datastructure['npos']
    # Nprev_pos = datastructure['Nprev_pos']
    Nprev_br  = datastructure['Nprev_br']
    nbr = datastructure['nbr']
    # Nprev_brb = datastructure['Nprev_brb']
    # Tbrb = datastructure['Tbrb']
    # nbrb = datastructure['nbrb']

    app_input = Input(shape=(napp,), name='app_input')
    prev_input = Input(shape=(Tpr, npr), name='prev_input')
    cred_input = Input(shape=(Nprev_cred, Tcred, ncred), name='cred_input')
    # inst_input = Input(shape=(Nprev_inst, Tinst, ninst), name='inst_input')
    # pos_input  = Input(shape=(Nprev_pos,  Tpos,  npos ), name='pos_input')
    bure_input = Input(shape=(Nprev_br, nbr), name='bure_input')

    ker_init1 = keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0)

    # if neglectBureauBal==False:
    #     bure_bal_input = Input(shape=(Nprev_brb, Tbrb, nbrb), name='bure_bal_input')

    prev_info = LSTM(128, kernel_initializer=ker_init1, name='lstm_prev')(prev_input)
    # prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_1')(prev_info)
    # prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_2')(prev_info)

    
    cred_info = TimeDistributed(
                        LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_cred_1')
                , name='timedist_cred')(cred_input)
    cred_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_2'))(cred_info)
    cred_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_3')(cred_info)
    # cred_info = Dropout(0.5)(cred_info)
    cred_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_cred_1')(cred_info)

    
    # inst_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1, name='lstm_inst_1'), name='timedist_inst')(inst_input)
    # inst_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_2'))(inst_info)
    # inst_info = LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_3')(inst_info)
    # inst_info = Dropout(0.5)(inst_info)
    # inst_info = Dense(16, kernel_initializer=ker_init1, activation='relu', name='dense_inst_1')(inst_info)


    # pos_info = TimeDistributed(LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_pos_1'), name='timedist_pos')(pos_input)
    # pos_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_2'))(pos_info)
    # pos_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_3')(pos_info)
    # pos_info = Dropout(0.5)(pos_info)
    # pos_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_pos_1')(pos_info)



    bureau = LSTM(64, return_sequences=True, kernel_initializer=ker_init1, name='lstm_bure_0')(bure_input)
    bureau = LSTM(64, kernel_initializer=ker_init1, name='lstm_bure_1')(bureau)
    # bureau = Dropout(0.5)(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_1')(bureau)

    # if neglectBureauBal==False:
    #     bure_bal_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1,
    #                      name='lstm_bure_bal_1'), name='timedist_brbal')(bure_bal_input)
    #     bure_bal_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_bure_bal_2'))(bure_bal_info)
    #     bure_bal_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_bure_bal_3')(bure_bal_info)
    #     bure_bal_info = Dropout(0.5)(bure_bal_info)
    #     bure_bal_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_bure_bal_1')(bure_bal_info)

    #     bureau = Concatenate()([bureau, bure_bal_info])


    # x = Concatenate()([app_input, prev_info, cred_info, inst_info, pos_info, bureau])
    x = Concatenate()([app_input, prev_info, cred_info, bureau])
    x = Dense(512, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0')(x)
    # x = Dropout(0.5)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1')(x)
    # x = Dropout(0.5)(x)
    x = Dense(128, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_2')(x)
    # x = Dropout(0.5)(x)
    x = Dense(32, activation='relu',  kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_3')(x)
    # x = Dropout(0.5)(x)
    x = Dense(1, activation='sigmoid',kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),  name='dense_4')(x)

    # if neglectBureauBal==False:
    #     inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input, bure_bal_input]
    # else:
    inputs = [app_input, prev_input, cred_input, bure_input]
    model = keras.models.Model(inputs=inputs, outputs=x)

    return model


def buildModel_v4(datastructure):
    Input = keras.layers.Input
    LSTM = keras.layers.LSTM
    TimeDistributed = keras.layers.TimeDistributed
    Dense = keras.layers.Dense
    Model = keras.models.Model
    Concatenate = keras.layers.Concatenate
    Dropout = keras.layers.Dropout

    napp = datastructure['napp']
    Tpr = datastructure['Tpr']
    npr = datastructure['npr']
    Tcred = datastructure['Tcred']
    ncred = datastructure['ncred']
    Nprev_cred = datastructure['Nprev_cred']
    # Tinst = datastructure['Tinst']
    # ninst = datastructure['ninst']
    # Nprev_inst = datastructure['Nprev_inst']
    # Tpos = datastructure['Tpos']
    # npos = datastructure['npos']
    # Nprev_pos = datastructure['Nprev_pos']
    Nprev_br  = datastructure['Nprev_br']
    nbr = datastructure['nbr']
    # Nprev_brb = datastructure['Nprev_brb']
    # Tbrb = datastructure['Tbrb']
    # nbrb = datastructure['nbrb']

    app_input = Input(shape=(napp,), name='app_input')
    prev_input = Input(shape=(Tpr, npr), name='prev_input')
    cred_input = Input(shape=(Nprev_cred, Tcred, ncred), name='cred_input')
    # inst_input = Input(shape=(Nprev_inst, Tinst, ninst), name='inst_input')
    # pos_input  = Input(shape=(Nprev_pos,  Tpos,  npos ), name='pos_input')
    bure_input = Input(shape=(Nprev_br, nbr), name='bure_input')

    ker_init1 = keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0)

    # if neglectBureauBal==False:
    #     bure_bal_input = Input(shape=(Nprev_brb, Tbrb, nbrb), name='bure_bal_input')

    prev_info = LSTM(128, kernel_initializer=ker_init1, name='lstm_prev')(prev_input)
    prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_1')(prev_info)
    prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_2')(prev_info)

    
    cred_info = TimeDistributed(
                        LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_cred_1')
                , name='timedist_cred')(cred_input)
    cred_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_2'))(cred_info)
    cred_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_3')(cred_info)
    cred_info = Dropout(0.5)(cred_info)
    cred_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_cred_1')(cred_info)

    
    # inst_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1, name='lstm_inst_1'), name='timedist_inst')(inst_input)
    # inst_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_2'))(inst_info)
    # inst_info = LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_3')(inst_info)
    # inst_info = Dropout(0.5)(inst_info)
    # inst_info = Dense(16, kernel_initializer=ker_init1, activation='relu', name='dense_inst_1')(inst_info)


    # pos_info = TimeDistributed(LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_pos_1'), name='timedist_pos')(pos_input)
    # pos_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_2'))(pos_info)
    # pos_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_3')(pos_info)
    # pos_info = Dropout(0.5)(pos_info)
    # pos_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_pos_1')(pos_info)



    bureau = LSTM(64, return_sequences=True, kernel_initializer=ker_init1, name='lstm_bure_0')(bure_input)
    bureau = LSTM(64, kernel_initializer=ker_init1, name='lstm_bure_1')(bureau)
    bureau = Dropout(0.5)(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_1')(bureau)

    # if neglectBureauBal==False:
    #     bure_bal_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1,
    #                      name='lstm_bure_bal_1'), name='timedist_brbal')(bure_bal_input)
    #     bure_bal_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_bure_bal_2'))(bure_bal_info)
    #     bure_bal_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_bure_bal_3')(bure_bal_info)
    #     bure_bal_info = Dropout(0.5)(bure_bal_info)
    #     bure_bal_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_bure_bal_1')(bure_bal_info)

    #     bureau = Concatenate()([bureau, bure_bal_info])


    # x = Concatenate()([app_input, prev_info, cred_info, inst_info, pos_info, bureau])
    x = Concatenate()([app_input, prev_info, cred_info, bureau])
    x = Dense(512, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0')(x)
    x = Dropout(0.5)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1')(x)
    x = Dropout(0.5)(x)
    x = Dense(128, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_2')(x)
    x = Dropout(0.5)(x)
    x = Dense(32, activation='relu',  kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_3')(x)
    x = Dropout(0.5)(x)
    x = Dense(1, activation='sigmoid',kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),  name='dense_4')(x)

    # if neglectBureauBal==False:
    #     inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input, bure_bal_input]
    # else:
    inputs = [app_input, prev_input, cred_input, bure_input]
    model = keras.models.Model(inputs=inputs, outputs=x)

    return model


def buildModel_v3(datastructure, neglectBureauBal=True):
    Input = keras.layers.Input
    LSTM = keras.layers.LSTM
    TimeDistributed = keras.layers.TimeDistributed
    Dense = keras.layers.Dense
    Model = keras.models.Model
    Concatenate = keras.layers.Concatenate
    Dropout = keras.layers.Dropout

    napp = datastructure['napp']
    Tpr = datastructure['Tpr']
    npr = datastructure['npr']
    Tcred = datastructure['Tcred']
    ncred = datastructure['ncred']
    Nprev_cred = datastructure['Nprev_cred']
    Tinst = datastructure['Tinst']
    ninst = datastructure['ninst']
    Nprev_inst = datastructure['Nprev_inst']
    Tpos = datastructure['Tpos']
    npos = datastructure['npos']
    Nprev_pos = datastructure['Nprev_pos']
    Nprev_br  = datastructure['Nprev_br']
    nbr = datastructure['nbr']
    Nprev_brb = datastructure['Nprev_brb']
    Tbrb = datastructure['Tbrb']
    nbrb = datastructure['nbrb']

    app_input = Input(shape=(napp,), name='app_input')
    prev_input = Input(shape=(Tpr, npr), name='prev_input')
    cred_input = Input(shape=(Nprev_cred, Tcred, ncred), name='cred_input')
    inst_input = Input(shape=(Nprev_inst, Tinst, ninst), name='inst_input')
    pos_input  = Input(shape=(Nprev_pos,  Tpos,  npos ), name='pos_input')
    bure_input = Input(shape=(Nprev_br, nbr), name='bure_input')

    ker_init1 = keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0)

    if neglectBureauBal==False:
        bure_bal_input = Input(shape=(Nprev_brb, Tbrb, nbrb), name='bure_bal_input')

    prev_info = LSTM(128, kernel_initializer=ker_init1, name='lstm_prev')(prev_input)
    prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_1')(prev_info)
    prev_info = Dropout(0.5)(prev_info)
    prev_info = Dense(64, activation='relu', kernel_initializer=ker_init1, name='dense_prev_2')(prev_info)

    
    cred_info = TimeDistributed(
                        LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_cred_1')
                , name='timedist_cred')(cred_input)
    cred_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_2'))(cred_info)
    cred_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_cred_3')(cred_info)
    cred_info = Dropout(0.5)(cred_info)
    cred_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_cred_1')(cred_info)

    
    inst_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1, name='lstm_inst_1'), name='timedist_inst')(inst_input)
    inst_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_2'))(inst_info)
    inst_info = LSTM(16, kernel_initializer=ker_init1, name='lstm_inst_3')(inst_info)
    inst_info = Dropout(0.5)(inst_info)
    inst_info = Dense(16, kernel_initializer=ker_init1, activation='relu', name='dense_inst_1')(inst_info)


    pos_info = TimeDistributed(LSTM(32, return_sequences=True, kernel_initializer=ker_init1, name='lstm_pos_1'), name='timedist_pos')(pos_input)
    pos_info = TimeDistributed(LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_2'))(pos_info)
    pos_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_pos_3')(pos_info)
    pos_info = Dropout(0.5)(pos_info)
    pos_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_pos_1')(pos_info)



    bureau = LSTM(64, return_sequences=True, kernel_initializer=ker_init1, name='lstm_bure_0')(bure_input)
    bureau = LSTM(64, kernel_initializer=ker_init1, name='lstm_bure_1')(bureau)
    bureau = Dropout(0.5)(bureau)
    bureau = Dense(64, kernel_initializer=ker_init1, activation='relu', name='dense_bureau_1')(bureau)

    if neglectBureauBal==False:
        bure_bal_info = TimeDistributed(LSTM(16, return_sequences=True, kernel_initializer=ker_init1,
                         name='lstm_bure_bal_1'), name='timedist_brbal')(bure_bal_input)
        bure_bal_info = TimeDistributed(LSTM(16, kernel_initializer=ker_init1, name='lstm_bure_bal_2'))(bure_bal_info)
        bure_bal_info = LSTM(32, kernel_initializer=ker_init1, name='lstm_bure_bal_3')(bure_bal_info)
        bure_bal_info = Dropout(0.5)(bure_bal_info)
        bure_bal_info = Dense(32, kernel_initializer=ker_init1, activation='relu', name='dense_bure_bal_1')(bure_bal_info)

        bureau = Concatenate()([bureau, bure_bal_info])


    x = Concatenate()([app_input, prev_info, cred_info, inst_info, pos_info, bureau])
    x = Dense(512, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0')(x)
    x = Dropout(0.5)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1')(x)
    x = Dropout(0.5)(x)
    x = Dense(128, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_2')(x)
    x = Dropout(0.5)(x)
    x = Dense(32, activation='relu',  kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_3')(x)
    x = Dropout(0.5)(x)
    x = Dense(1, activation='sigmoid',kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),  name='dense_4')(x)

    if neglectBureauBal==False:
        inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input, bure_bal_input]
    else:
        inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input]
    model = keras.models.Model(inputs=inputs, outputs=x)

    return model


def buildModel_v2(datastructure, neglectBureauBal=True):
    Input = keras.layers.Input
    LSTM = keras.layers.LSTM
    TimeDistributed = keras.layers.TimeDistributed
    Dense = keras.layers.Dense
    Model = keras.models.Model
    Concatenate = keras.layers.Concatenate
    Dropout = keras.layers.Dropout

    napp = datastructure['napp']
    Tpr = datastructure['Tpr']
    npr = datastructure['npr']
    Tcred = datastructure['Tcred']
    ncred = datastructure['ncred']
    Nprev_cred = datastructure['Nprev_cred']
    Tinst = datastructure['Tinst']
    ninst = datastructure['ninst']
    Nprev_inst = datastructure['Nprev_inst']
    Tpos = datastructure['Tpos']
    npos = datastructure['npos']
    Nprev_pos = datastructure['Nprev_pos']
    Nprev_br  = datastructure['Nprev_br']
    nbr = datastructure['nbr']
    Nprev_brb = datastructure['Nprev_brb']
    Tbrb = datastructure['Tbrb']
    nbrb = datastructure['nbrb']

    app_input = Input(shape=(napp,), name='app_input')
    prev_input = Input(shape=(Tpr, npr), name='prev_input')
    cred_input = Input(shape=(Nprev_cred, Tcred, ncred), name='cred_input')
    inst_input = Input(shape=(Nprev_inst, Tinst, ninst), name='inst_input')
    pos_input  = Input(shape=(Nprev_pos,  Tpos,  npos ), name='pos_input')
    bure_input = Input(shape=(Nprev_br, nbr), name='bure_input')

    if neglectBureauBal==False:
        bure_bal_input = Input(shape=(Nprev_brb, Tbrb, nbrb), name='bure_bal_input')

    prev_info = LSTM(64, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                    name='lstm_prev')(prev_input)

    cred_info = TimeDistributed(LSTM(32, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                    name='lstm_cred_1'))(cred_input)
    cred_info = LSTM(32, name='lstm_cred_2')(cred_info)

    inst_info = TimeDistributed(LSTM(32, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                    name='lstm_inst_1'))(inst_input)
    inst_info = LSTM(32, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), 
                    name='lstm_inst_2')(inst_info)

    pos_info = TimeDistributed(LSTM(32, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                    name='lstm_pos_1'))(pos_input)
    pos_info = LSTM(64, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), 
                    name='lstm_pos_2')(pos_info)

    bureau = LSTM(64, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                    name='lstm_bure')(bure_input)

    if neglectBureauBal==False:
        bure_bal_info = TimeDistributed(LSTM(8, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                         name='lstm_bure_bal_1'))(bure_bal_input)
        bure_bal_info = LSTM(32, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                        name='lstm_bure_bal_2')(bure_bal_info)

        bureau = Concatenate()([bure_info, bure_bal_info])
        bureau = Dense(32, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_bureau_1')(bureau)

    x = Concatenate()([app_input, prev_info, cred_info, inst_info, pos_info, bureau])
    x = Dense(512, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0a')(x)
    x = Dropout(0.5)(x)
    x = Dense(256, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_0b')(x)
    x = Dropout(0.5)(x)
    x = Dense(128, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1')(x)
    x = Dropout(0.5)(x)
    x = Dense(32, activation='relu',  kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_2')(x)
    x = Dropout(0.5)(x)
    x = Dense(1, activation='sigmoid',kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),  name='dense_3')(x)

    if neglectBureauBal==False:
        inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input, bure_bal_input]
    else:
        inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input]
    model = keras.models.Model(inputs=inputs, outputs=x)

    return model


def buildModel_v1(datastructure, neglectBureauBal=True):
    Input = keras.layers.Input
    LSTM = keras.layers.LSTM
    TimeDistributed = keras.layers.TimeDistributed
    Dense = keras.layers.Dense
    Model = keras.models.Model
    Concatenate = keras.layers.Concatenate
    Dropout = keras.layers.Dropout

    napp = datastructure['napp']
    Tpr = datastructure['Tpr']
    npr = datastructure['npr']
    Tcred = datastructure['Tcred']
    ncred = datastructure['ncred']
    Nprev_cred = datastructure['Nprev_cred']
    Tinst = datastructure['Tinst']
    ninst = datastructure['ninst']
    Nprev_inst = datastructure['Nprev_inst']
    Tpos = datastructure['Tpos']
    npos = datastructure['npos']
    Nprev_pos = datastructure['Nprev_pos']
    Nprev_br  = datastructure['Nprev_br']
    nbr = datastructure['nbr']
    Nprev_brb = datastructure['Nprev_brb']
    Tbrb = datastructure['Tbrb']
    nbrb = datastructure['nbrb']

    app_input = Input(shape=(napp,), name='app_input')
    prev_input = Input(shape=(Tpr, npr), name='prev_input')
    cred_input = Input(shape=(Nprev_cred, Tcred, ncred), name='cred_input')
    inst_input = Input(shape=(Nprev_inst, Tinst, ninst), name='inst_input')
    pos_input  = Input(shape=(Nprev_pos,  Tpos,  npos ), name='pos_input')
    bure_input = Input(shape=(Nprev_br, nbr), name='bure_input')

    if neglectBureauBal==False:
        bure_bal_input = Input(shape=(Nprev_brb, Tbrb, nbrb), name='bure_bal_input')

    prev_info = LSTM(32, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                    name='lstm_prev')(prev_input)

    cred_info = TimeDistributed(LSTM(32*2, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                    name='lstm_cred_1'))(cred_input)
    cred_info = LSTM(32, name='lstm_cred_2')(cred_info)

    inst_info = TimeDistributed(LSTM(32*2, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                    name='lstm_inst_1'))(inst_input)
    inst_info = LSTM(32, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), 
                    name='lstm_inst_2')(inst_info)

    pos_info = TimeDistributed(LSTM(32*2, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                    name='lstm_pos_1'))(pos_input)
    pos_info = LSTM(32, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), 
                    name='lstm_pos_2')(pos_info)

    bureau = LSTM(32, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                    name='lstm_bure')(bure_input)

    if neglectBureauBal==False:
        bure_bal_info = TimeDistributed(LSTM(32, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                         name='lstm_bure_bal_1'))(bure_bal_input)
        bure_bal_info = LSTM(32, kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),
                        name='lstm_bure_bal_2')(bure_bal_info)

        bureau = Concatenate()([bure_info, bure_bal_info])
        bureau = Dense(32, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_bureau_1')(bureau)

    x = Concatenate()([app_input, prev_info, cred_info, inst_info, pos_info, bureau])
    x = Dense(128, activation='relu', kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_1')(x)
    x = Dropout(0.5)(x)
    x = Dense(32, activation='relu',  kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0), name='dense_2')(x)
    x = Dropout(0.5)(x)
    x = Dense(1, activation='sigmoid',kernel_initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=0),  name='dense_3')(x)

    if neglectBureauBal==False:
        inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input, bure_bal_input]
    else:
        inputs = [app_input, prev_input, cred_input, inst_input, pos_input, bure_input]
    model = keras.models.Model(inputs=inputs, outputs=x)

    return model


def prepareTestSet(app_data, label_dict):

    app_data_cols = ['SK_ID_CURR', 'NAME_CONTRACT_TYPE', 'CODE_GENDER',
    'FLAG_OWN_CAR', 'FLAG_OWN_REALTY', 'CNT_CHILDREN',
    'AMT_INCOME_TOTAL', 'AMT_CREDIT', 'AMT_ANNUITY', 'AMT_GOODS_PRICE',
    'NAME_INCOME_TYPE', 'NAME_EDUCATION_TYPE', 'NAME_FAMILY_STATUS',
    'NAME_HOUSING_TYPE', 'DAYS_BIRTH', 'DAYS_EMPLOYED', 'OCCUPATION_TYPE']

    app_data = app_data[app_data_cols].copy(True)
    app_data = correctApplicationData(app_data)
    app_data.pipe(convertObjToLabel, label_dict)
    onehot_dict = getOneHot(app_data, label_dict)

    return (app_data, label_dict, onehot_dict)


def massageDatasets(datasets):

    if 'app_data' in datasets.keys():
        app_data = datasets['app_data']
    if 'prev_app_data' in datasets.keys():
        prev_app_data = datasets['prev_app_data']
    if 'cred_bal_data' in datasets.keys():
        cred_bal_data = datasets['cred_bal_data']
    if 'inst_pay_data' in datasets.keys():
        inst_pay_data = datasets['inst_pay_data']
    if 'pos_cash_data' in datasets.keys():
        pos_cash_data = datasets['pos_cash_data']
    if 'bureau_data' in datasets.keys():
        bureau_data   = datasets['bureau_data']
    if 'bureau_bal_data' in datasets.keys():
        bureau_bal_data = datasets['bureau_bal_data']


    # label_dicts
    labelDicts = dict()
    # onehot
    onehots = dict()
    # norm cols
    normCols = dict()
    # norm cols
    normFactors = dict()




    if 'app_data' in datasets.keys():
        app_data_cols = ['SK_ID_CURR', 'TARGET', 'NAME_CONTRACT_TYPE', 'CODE_GENDER',
            'FLAG_OWN_CAR', 'FLAG_OWN_REALTY', 'CNT_CHILDREN',
            'AMT_INCOME_TOTAL', 'AMT_CREDIT', 'AMT_ANNUITY', 'AMT_GOODS_PRICE',
            'NAME_INCOME_TYPE', 'NAME_EDUCATION_TYPE', 'NAME_FAMILY_STATUS',
            'NAME_HOUSING_TYPE', 'DAYS_BIRTH', 'DAYS_EMPLOYED', 'OCCUPATION_TYPE']


        app_data = app_data[app_data_cols].copy(True)
        app_data = correctApplicationData(app_data)
        app_label_dict = app_data.pipe(getLabelDict)
        app_data.pipe(convertObjToLabel, app_label_dict)
        app_data_onehot_dict = getOneHot(app_data, app_label_dict)
        app_data_norm_cols = ['AMT_INCOME_TOTAL', 'AMT_CREDIT', 'AMT_ANNUITY', 'AMT_GOODS_PRICE', 'DAYS_BIRTH', 'DAYS_EMPLOYED']
        app_data_norm_factors = getNormFactors(app_data, app_data_norm_cols)

        datasets['app_data'] = app_data
        labelDicts['app_data'] = app_label_dict
        onehots['app_data'] = app_data_onehot_dict
        normCols['app_data'] = app_data_norm_cols
        normFactors['app_data'] = app_data_norm_factors


    if 'prev_app_data' in datasets.keys():
        prev_data_cols = ['SK_ID_PREV', 'SK_ID_CURR', 'NAME_CONTRACT_TYPE', 'AMT_APPLICATION', 'AMT_CREDIT',
            'FLAG_LAST_APPL_PER_CONTRACT', 'NFLAG_LAST_APPL_IN_DAY',
            'NAME_CONTRACT_STATUS', 'DAYS_DECISION', 'NAME_PAYMENT_TYPE', 'CODE_REJECT_REASON', 'NAME_TYPE_SUITE',
            'NAME_CLIENT_TYPE', 'NAME_PORTFOLIO', 'NAME_SELLER_INDUSTRY',
            'CNT_PAYMENT', 'NAME_YIELD_GROUP', 'NFLAG_INSURED_ON_APPROVAL',
            'DAYS_TERMINATION']
        prev_app_data = prev_app_data[prev_data_cols].copy(True)
        prev_app_data = correctPrevAppData(prev_app_data)
        prev_label_dict = prev_app_data.pipe(getLabelDict)
        prev_app_data.pipe(convertObjToLabel, prev_label_dict)
        prev_data_norm_cols = ['AMT_APPLICATION', 'AMT_CREDIT']
        prev_data_onehot_dict = getOneHot(prev_app_data, prev_label_dict)
        prev_data_norm_factors = getNormFactors(prev_app_data, prev_data_norm_cols)

        datasets['prev_app_data'] = prev_app_data
        labelDicts['prev_app_data'] = prev_label_dict
        onehots['prev_app_data'] = prev_data_onehot_dict
        normCols['prev_app_data'] = prev_data_norm_cols
        normFactors['prev_app_data'] = prev_data_norm_factors


    if 'cred_bal_data' in datasets.keys():
        cred_data_cols = ['SK_ID_CURR', 'SK_ID_PREV', 'MONTHS_BALANCE', 'AMT_BALANCE',
            'AMT_CREDIT_LIMIT_ACTUAL', 'AMT_DRAWINGS_CURRENT', 'AMT_DRAWINGS_ATM_CURRENT',
            'AMT_DRAWINGS_OTHER_CURRENT', 'AMT_DRAWINGS_POS_CURRENT', 'AMT_INST_MIN_REGULARITY',
            'AMT_PAYMENT_CURRENT', 'AMT_PAYMENT_TOTAL_CURRENT',
            'AMT_RECEIVABLE_PRINCIPAL', 'AMT_RECIVABLE', 'AMT_TOTAL_RECEIVABLE',
            'CNT_INSTALMENT_MATURE_CUM', 'NAME_CONTRACT_STATUS', 'SK_DPD', 'SK_DPD_DEF']
        cred_bal_data = cred_bal_data[cred_data_cols].copy(True)
        cred_bal_data = correctCredData(cred_bal_data)
        cred_label_dict = cred_bal_data.pipe(getLabelDict)
        cred_bal_data.pipe(convertObjToLabel, cred_label_dict)
        cred_data_norm_cols = ['AMT_BALANCE',
            'AMT_CREDIT_LIMIT_ACTUAL', 'AMT_DRAWINGS_CURRENT', 'AMT_DRAWINGS_ATM_CURRENT',
            'AMT_DRAWINGS_OTHER_CURRENT', 'AMT_DRAWINGS_POS_CURRENT', 'AMT_INST_MIN_REGULARITY',
            'AMT_PAYMENT_CURRENT', 'AMT_PAYMENT_TOTAL_CURRENT',
            'AMT_RECEIVABLE_PRINCIPAL', 'AMT_RECIVABLE', 'AMT_TOTAL_RECEIVABLE']
        cred_data_onehot_dict = getOneHot(cred_bal_data, cred_label_dict)
        cred_data_norm_factors = getNormFactors(cred_bal_data, cred_data_norm_cols)

        datasets['cred_bal_data'] = cred_bal_data
        labelDicts['cred_bal_data'] = cred_label_dict
        onehots['cred_bal_data'] = cred_data_onehot_dict
        normCols['cred_bal_data'] = cred_data_norm_cols
        normFactors['cred_bal_data'] = cred_data_norm_factors
    


    if 'inst_pay_data' in datasets.keys():
        inst_data_cols = ['SK_ID_CURR', 'SK_ID_PREV', 'NUM_INSTALMENT_VERSION',
               'NUM_INSTALMENT_NUMBER', 'DAYS_INSTALMENT', 'DAYS_ENTRY_PAYMENT',
               'AMT_INSTALMENT', 'AMT_PAYMENT']
        inst_pay_data = inst_pay_data[inst_data_cols].copy(True)
        inst_pay_data = correctInstData(inst_pay_data)
        inst_label_dict = cred_bal_data.pipe(getLabelDict)
        inst_pay_data.pipe(convertObjToLabel, inst_label_dict)
        inst_data_norm_cols = ['AMT_INSTALMENT', 'AMT_PAYMENT']
        inst_data_norm_factors = getNormFactors(inst_pay_data, inst_data_norm_cols)
        inst_data_onehot_dict = getOneHot(inst_pay_data, inst_label_dict)

        datasets['inst_pay_data'] = inst_pay_data
        labelDicts['inst_pay_data'] = inst_label_dict
        onehots['inst_pay_data'] = inst_data_onehot_dict
        normCols['inst_pay_data'] = inst_data_norm_cols
        normFactors['inst_pay_data'] = inst_data_norm_factors
    


    if 'pos_cash_data' in datasets.keys():
        pos_data_cols = list(pos_cash_data.columns.values)
        pos_cash_data = pos_cash_data[pos_data_cols].copy(True)
        pos_cash_data = correctPOSData(pos_cash_data)
        pos_label_dict = pos_cash_data.pipe(getLabelDict)
        pos_cash_data.pipe(convertObjToLabel, pos_label_dict)
        pos_data_norm_cols = []
        pos_data_norm_factors = getNormFactors(pos_cash_data, pos_data_norm_cols)
        pos_data_onehot_dict = getOneHot(pos_cash_data, pos_label_dict)

        datasets['pos_cash_data'] = pos_cash_data
        labelDicts['pos_cash_data'] = pos_label_dict
        onehots['pos_cash_data'] = pos_data_onehot_dict
        normCols['pos_cash_data'] = pos_data_norm_cols
        normFactors['pos_cash_data'] = pos_data_norm_factors
    


    if 'bureau_data' in datasets.keys():
        bureau_data.sort_values('SK_ID_CURR', inplace=True)
        bureau_data_cols = list(bureau_data.columns.values)
        bureau_data = bureau_data[bureau_data_cols].copy(True)
        bureau_data = correctBureauData(bureau_data)
        bureau_label_dict = getLabelDict(bureau_data)
        bureau_data.pipe(convertObjToLabel, bureau_label_dict)
        bureau_norm_cols = ['AMT_CREDIT_MAX_OVERDUE', 'AMT_CREDIT_SUM', 'AMT_CREDIT_SUM_DEBT',
               'AMT_CREDIT_SUM_LIMIT', 'AMT_CREDIT_SUM_OVERDUE']
        bureau_norm_factors = getNormFactors(bureau_data, bureau_norm_cols)
        bureau_onehot_dict = getOneHot(bureau_data, bureau_label_dict)

        datasets['bureau_data']   = bureau_data
        labelDicts['bureau_data']   = bureau_label_dict
        onehots['bureau_data']   = bureau_onehot_dict
        normCols['bureau_data']   = bureau_norm_cols
        normFactors['bureau_data']   = bureau_norm_factors
    


    if 'bureau_bal_data' in datasets.keys():
        bureau_bal_data_cols = list(bureau_bal_data.columns.values)
        bureau_bal_data = correctBureauBalData(bureau_bal_data)
        bureau_bal_label_dict = getLabelDict(bureau_bal_data)
        bureau_bal_data.pipe(convertObjToLabel, bureau_bal_label_dict)
        bureau_bal_norm_cols = []
        bureau_bal_norm_factors = getNormFactors(bureau_bal_data, bureau_bal_norm_cols)
        bureau_bal_onehot_dict = getOneHot(bureau_bal_data, bureau_bal_label_dict)

        datasets['bureau_bal_data'] = bureau_bal_data
        labelDicts['bureau_bal_data'] = bureau_bal_label_dict
        onehots['bureau_bal_data'] = bureau_bal_onehot_dict
        normCols['bureau_bal_data'] = bureau_bal_norm_cols
        normFactors['bureau_bal_data'] = bureau_bal_norm_factors


    caches = dict()
    caches['labels'] = labelDicts
    caches['onehots'] = onehots
    caches['norm_cols'] = normCols
    caches['norm_factors'] = normFactors

    return (datasets, caches)


def getDataStructure(datasets, caches):

    datastructure = dict()

    if 'app_data' in datasets.keys():
        app_data = datasets['app_data']
        datastructure['napp'] = NColumnOnehot(app_data, caches['onehots']['app_data']) - 2

    if 'prev_app_data' in datasets.keys():
        prev_app_data = datasets['prev_app_data']
        datastructure['Tpr'] = 8
        datastructure['npr'] = NColumnOnehot(prev_app_data, caches['onehots']['prev_app_data']) - 2

    if 'cred_bal_data' in datasets.keys():
        cred_bal_data = datasets['cred_bal_data']
        datastructure['Tcred'] = 96
        datastructure['ncred'] = NColumnOnehot(cred_bal_data, caches['onehots']['cred_bal_data']) - 2
        datastructure['Nprev_cred'] = 4

    if 'inst_pay_data' in datasets.keys():
        inst_pay_data = datasets['inst_pay_data']
        datastructure['Tinst'] = 293
        datastructure['ninst'] = NColumnOnehot(inst_pay_data, caches['onehots']['inst_pay_data']) - 2
        datastructure['Nprev_inst'] = 26

    if 'pos_cash_data' in datasets.keys():
        pos_cash_data = datasets['pos_cash_data']
        datastructure['Tpos'] = 96
        datastructure['npos'] = NColumnOnehot(pos_cash_data, caches['onehots']['pos_cash_data']) - 2
        datastructure['Nprev_pos'] = 26

    if 'bureau_data' in datasets.keys():
        bureau_data   = datasets['bureau_data']
        datastructure['Nprev_br'] = 116
        datastructure['nbr'] = NColumnOnehot(bureau_data, caches['onehots']['bureau_data']) - 2

    if 'bureau_bal_data' in datasets.keys():
        bureau_bal_data = datasets['bureau_bal_data']
        datastructure['Nprev_brb'] = 116
        datastructure['Tbrb'] = 97
        datastructure['nbrb'] = NColumnOnehot(bureau_bal_data, caches['onehots']['bureau_bal_data']) - 1

    

    return datastructure


def summarizeColumns(dataframe, filename):
    cols = dataframe.columns.values
    summary = pd.DataFrame(columns=['dtype', 'unique num', 'null %']
                                      , index=dataframe.columns.values)

    N = dataframe.shape[0]

    for col in cols:
        summary.loc[col, 'dtype'] = dataframe[col].dtype
        summary.loc[col, 'unique num'] = dataframe[col].unique().size
        summary.loc[col,'null %'] = dataframe[col].isnull().sum()/N*100

    summary.to_csv(filename)


def getLabelDict(dataframe):
    """
    Return a dictionary of object data type to a numeric value
    """
    output = dict()
    obj_cols = dataframe.select_dtypes('object').columns.values # a numpy array
    
    for col in obj_cols:
        
        unique_vals = dataframe[col].unique()
        N = unique_vals.size
        
        label = dict()
        count = 0
        for val in unique_vals:
            label[val] = count
            count += 1
        
        output[col] = label
        
    return output


def convertObjToLabel(df, labelDict, verbose=False):
    """
    Convert an 'object' to a number label from labelDict
    """
    for col_name, enc_dict in labelDict.items():
        for value, label in enc_dict.items():
            if verbose==True:
                print('In {}, replace {} with {}'.format(col_name, value, label))
            df[col_name].replace(value, label, inplace=True)
    return df


def getOneHot(df, labelDict):
    """
    Return a dictionary with resultant one hot vectors
    """
    onehot = dict()
    for col_name, enc_dict in labelDict.items():
        vals = df[col_name].values
        converted = keras.utils.to_categorical(vals, len(enc_dict))
        converted = pd.DataFrame(converted, index=df.index)
        # onehot[col_name] = keras.utils.to_categorical(vals, len(enc_dict))
        onehot[col_name] = converted

    return onehot


def getNormFactors(df, cols):
    """
    Return a dictionary with column names as keys and (mean, std) tuple as value.
    """
    normFactors = dict()
    for col in cols:
        vals = df[col].values
        normFactors[col] = (np.mean(vals), np.std(vals))
    return normFactors


def NColumnOnehot(df, ohdict):
    n_col = df.shape[1]
    n_new_col = 0
    for key, val in ohdict.items():
        n_new_col += val.shape[1]-1

    N = n_col+n_new_col
    return N


def convertDFtoNP(df, ohdict, normdict):
    n_col = df.shape[1]
    n_new_col = 0
    for key, val in ohdict.items():
        n_new_col += val.shape[1]-1

    N = n_col+n_new_col
    Nrow = df.shape[0]
    data = np.zeros((Nrow, N), dtype=np.float)

    cols = df.columns.values
    nextcolid = 0

    idx = df.index

    if idx.size>0:
        for col in cols:
            # print('{}, {}'.format(col, nextcolid))
            if col in list(ohdict.keys()):
                size = ohdict[col].shape[1]
                data[:, nextcolid:nextcolid+size] = ohdict[col].loc[idx,:]
                nextcolid += size
                continue
            if col in list(normdict.keys()):
                org_val = df[col].values
                mean, std = normdict[col]
                data[:, nextcolid] = (org_val-mean)/std
                nextcolid += 1
                continue
            else:
                org_val = df[col].values
                data[:, nextcolid] = org_val
                nextcolid += 1
                continue
            
    return data


def correctApplicationData(data):
    """
    data -- a dataframe
    """

    # 'CODE_GENDER'. Change 'XNA' to 'F'
    data.loc[data['CODE_GENDER']=='XNA','CODE_GENDER'] = 'F'

    # 'NAME_FAMILY_STATUS'. Change 'Unknown' to 'Single / not married'
    col = 'NAME_FAMILY_STATUS'
    data.loc[ data[col] == 'Unknown', col] = 'Single / not married'

    # 'AMT_ANNUITY'. Missing data will be replaced with the mean value.
    col = 'AMT_ANNUITY'
    meanval = data[col].mean()
    data.loc[ data[col].isna(), col] = meanval

    # 'AMT_GOODS_PRICE'. Missing data will be replaced with the mean value.
    col = 'AMT_GOODS_PRICE'
    meanval = data[col].mean()
    data.loc[ data[col].isna(), col] = meanval

    # 'DAYS_EMPLOYED'. Remove data points that >300,000 (which is equivalent to 1000 years, stupid value)
    col = 'DAYS_EMPLOYED'
    meanval = data.loc[data[col]<300000, col].mean()
    data.loc[ data[col]>=300000, col] = meanval

    # 'OCCUPATION_TYPE'. Change NaN to 'Unknown', since it accounts for a significant portion.
    col = 'OCCUPATION_TYPE'
    data.loc[ data[col].isnull(), col] = 'Unknown'

    return data


def correctPrevAppData(data):
    # 'NAME_CONTRACT_STATUS'. Only select Accepted and Resufed only.
    data = data.loc[ data['NAME_CONTRACT_STATUS'].isin(['Approved', 'Refused'])].copy(True)

    col = 'AMT_CREDIT'
    data.loc[ data[col].isnull(), col] = 0

    # Throw away data that are by mistake from 'FLAG_LAST_APPL_PER_CONTRACT'
    col = 'FLAG_LAST_APPL_PER_CONTRACT'
    data = data.loc[ data[col] == 'Y', :].copy(True)

    # Throw away data that are not the last of the day
    col = 'NFLAG_LAST_APPL_IN_DAY'
    data = data.loc[ data[col] == 1, :].copy(True)

    # 'CNT_PAYMENT', replace NaN with -1000
    col = 'CNT_PAYMENT'
    data.loc[ data[col].isnull(), col ] = -1000

    # 'DAYS_TERMINATION', replace NaN with 10000
    col = 'DAYS_TERMINATION'
    data.loc[ data[col].isnull(), col ] = 10000
    # 'DAYS_TERMINATION', replace 365243 with 100
    data.loc[ data[col]==365243, col ]  = 100

    # 'NFLAG_INSURED_ON_APPROVAL', replace NaN with -100
    col = 'NFLAG_INSURED_ON_APPROVAL'
    data.loc[ data[col].isnull(), col] = -100

    return data


def correctCredData(data):
    # Replace NaN with 0
    col = 'AMT_DRAWINGS_ATM_CURRENT'
    data.loc[ data[col].isnull(), col ] = 0

    col = 'AMT_DRAWINGS_OTHER_CURRENT'
    data.loc[ data[col].isnull(), col ] = 0

    col = 'AMT_DRAWINGS_POS_CURRENT'
    data.loc[ data[col].isnull(), col ] = 0

    col = 'AMT_INST_MIN_REGULARITY'
    data.loc[ data[col].isnull(), col ] = 0

    col = 'AMT_PAYMENT_CURRENT'
    data.loc[ data[col].isnull(), col ] = 0

    col = 'CNT_INSTALMENT_MATURE_CUM'
    data.loc[ data[col].isnull(), col ] = -1000

    return data


def correctInstData(data):
    # Replace NaN with 10000
    col = 'DAYS_ENTRY_PAYMENT'
    data.loc[ data[col].isnull(), col] = 10000

    # Replace NaN with 0 for amount payment
    col = 'AMT_PAYMENT'
    data.loc[ data[col].isnull(), col] = 0

    return data


def correctPOSData(data):
    col = 'CNT_INSTALMENT'
    data.loc[ data[col].isnull(), col] = -10000

    col = 'CNT_INSTALMENT_FUTURE'
    data.loc[ data[col].isnull(), col] = -10000

    return data


def correctBureauData(data):
    col = 'CREDIT_DAY_OVERDUE'
    data.loc[ data[col].isnull(), col] = 0

    col = 'DAYS_ENDDATE_FACT'
    data.loc[ data[col].isnull(), col] = 42023 # set to the opposite of the min values

    col = 'DAYS_CREDIT_ENDDATE'
    data.loc[ data[col].isnull(), col] = 31199 # set t othe maximum

    col = 'AMT_CREDIT_MAX_OVERDUE'
    data.loc[ data[col].isnull(), col] = 0

    col = 'AMT_CREDIT_SUM'
    data.loc[ data[col].isnull(), col] = 0

    col = 'AMT_CREDIT_SUM_DEBT'
    data.loc[ data[col].isnull(), col] = 0

    col = 'AMT_CREDIT_SUM_LIMIT'
    data.loc[ data[col].isnull(), col] = 0

    col = 'AMT_ANNUITY'
    data.loc[ data[col].isnull(), col] = 0

    return data


def correctBureauBalData(data):

    return data


def as_keras_metric(method):
    """
    From:
    https://stackoverflow.com/questions/45947351/how-to-use-tensorflow-metrics-in-keras/50527423#50527423

    USAGE:
    auc_roc = as_keras_metric(tf.metrics.auc)

    Compile the keras model:

    model.compile(..., metrics=[auc_roc])
    Function decoration:

    Be aware of inconsistency with arguments (e.g. order of y_pred, y_true). Decorate your way out of it:

    @as_keras_metric
    def auc_roc(y_true, y_pred):
        return tf.contrib.metrics.streaming_auc(y_pred, y_true)
    You can also use the decorator to set default parameters (e.g. for mean_iou):

    @as_keras_metric
    def mean_iou(y_true, y_pred, num_classes=2):
        return tf.metrics.mean_iou(y_true, y_pred, num_classes)
    For metrics that returns multiple values I expect that the mean value is taken during each epoch evaluation. For instance, the use of two threshold values in precision_at_thresholds returns two values, but as far as I can see, Keras reports the average during training.

    @as_keras_metric
    def precision_at_thresholds(y_true, y_pred, thresholds=[0.25, 0.50]):
        return tf.metrics.precision_at_thresholds(y_true, y_pred, thresholds)
    Caveats:

    Be aware, that this is a hack. It might produce unwanted results, since Keras does not support tensorflow metrics.
    """
    import functools
    from keras import backend as K
    import tensorflow as tf
    @funcwraps(method)
    def wrapper(self, args, **kwargs):
        """ Wrapper for turning tensorflow metrics into keras metrics """
        value, update_op = method(self, args, **kwargs)
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([update_op]):
            value = tf.identity(value)
        return value
    return wrapper


def loss(y_true, y_pred):
    
    y_true = K.expand_dims(y_true, axis=-1)
    y_pred = K.pow(K.expand_dims(y_pred, axis=-1), 2)
    
    val1 = -K.dot(y_true, K.log(K.transpose(y_pred)))*10
    val2 = -K.dot(1-y_true, K.log(K.transpose(1-y_pred)))
    
    val = val1+val2
    
    return val


def f1_score(y_true, y_pred):
    y_true = tf.cast(y_true, dtype=tf.float32)
    y_pred = tf.cast(y_pred, dtype=tf.float32)
    y_correct = y_true * y_pred
    sum_true = tf.reduce_sum(y_true, axis=1)
    sum_pred = tf.reduce_sum(y_pred, axis=1)
    sum_correct = tf.reduce_sum(y_correct, axis=1)
    precision = sum_correct / sum_pred
    recall = sum_correct / sum_true
    f_score = 5 * precision * recall / (4 * precision + recall)
    f_score = tf.where(tf.is_nan(f_score), tf.zeros_like(f_score), f_score)
    return tf.reduce_mean(f_score)




